using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Mirror.Examples.Pong;

namespace Mirror.Examples.NetworkLobby
{
    public class NetworkLobbyManagerExt : NetworkLobbyManager
    {
        public Transform leftRacketSpawn;
        public Transform rightRacketSpawn;
        public GameObject ballPrefab;
        GameObject ball;

        /// <summary>
        /// Called just after GamePlayer object is instantiated and just before it replaces LobbyPlayer object.
        /// This is the ideal point to pass any data like player name, credentials, tokens, colors, etc.
        /// into the GamePlayer object as it is about to enter the Online scene.
        /// </summary>
        /// <param name="lobbyPlayer"></param>
        /// <param name="gamePlayer"></param>
        /// <returns>true unless some code in here decides it needs to abort the replacement</returns>
        public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
        {
            /*
            PlayerController player = gamePlayer.GetComponent<PlayerController>();
            player.Index = lobbyPlayer.GetComponent<NetworkLobbyPlayer>().Index;
            player.playerColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            */
            Player player = lobbyPlayer.GetComponent<Player>();
            NetworkLobbyPlayer nlp = gamePlayer.GetComponent<NetworkLobbyPlayer>();
            player.Index = nlp.Index;

            return true;
        }

        /*
            This code below is to demonstrate how to do a Start button that only appears for the Host player
            showStartButton is a local bool that's needed because OnLobbyServerPlayersReady is only fired when
            all players are ready, but if a player cancels their ready state there's no callback to set it back to false
            Therefore, allPlayersReady is used in combination with showStartButton to show/hide the Start button correctly.
            Setting showStartButton false when the button is pressed hides it in the game scene since NetworkLobbyManager
            is set as DontDestroyOnLoad = true.
        */

        bool showStartButton;

        public override void OnLobbyServerPlayersReady()
        {
            // calling the base method calls ServerChangeScene as soon as all players are in Ready state.
            if (SystemInfo.graphicsDeviceType == GraphicsDeviceType.Null && startOnHeadless)
                base.OnLobbyServerPlayersReady();
            else
                showStartButton = true;
        }

        public override void OnGUI()
        {
            base.OnGUI();

            if (allPlayersReady && showStartButton && GUI.Button(new Rect(150, 300, 120, 20), "START GAME"))
            {
                // set to false to hide it in the game scene
                showStartButton = false;

                ServerChangeScene(GameplayScene);
            }
        }

        public override void ServerChangeScene(string sceneName) {
            if (sceneName == LobbyScene) {
                foreach (NetworkLobbyPlayer lobbyPlayer in lobbySlots) {
                    if (lobbyPlayer == null) continue;

                    // find the game-player object for this connection, and destroy it
                    NetworkIdentity identity = lobbyPlayer.GetComponent<NetworkIdentity>();

                    NetworkIdentity playerController = identity.connectionToClient.playerController;
                    NetworkServer.Destroy(playerController.gameObject);

                    if (NetworkServer.active) {
                        // re-add the lobby object
                        lobbyPlayer.GetComponent<NetworkLobbyPlayer>().ReadyToBegin = false;
                        NetworkServer.ReplacePlayerForConnection(identity.connectionToClient, lobbyPlayer.gameObject);
                    }
                }
            }
            else {
                if (dontDestroyOnLoad) {
                    foreach (NetworkLobbyPlayer lobbyPlayer in lobbySlots) {
                        if (lobbyPlayer != null) {
                            lobbyPlayer.transform.SetParent(null);
                            DontDestroyOnLoad(lobbyPlayer);
                        }
                    }
                }
            }

            // network manager stuff
            string newSceneName = sceneName;
            if (string.IsNullOrEmpty(newSceneName)) {
                Debug.LogError("ServerChangeScene empty scene name");
                return;
            }

            if (LogFilter.Debug) Debug.Log("ServerChangeScene " + newSceneName);
            NetworkServer.SetAllClientsNotReady();
            networkSceneName = newSceneName;

            NetworkManager.loadingSceneAsync = SceneManager.LoadSceneAsync(newSceneName);

            SceneMessage msg = new SceneMessage(networkSceneName);
            NetworkServer.SendToAll(msg);
            
        }

        //
        public override void OnLobbyServerSceneChanged(string sceneName) {
            if (sceneName == "Scene 1") {
                GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

                lobbySlots[0].transform.position = gameManager.leftRacketSpawn.transform.position;
                lobbySlots[1].transform.position = gameManager.rightRacketSpawn.transform.position;

                /*
                if (lobbySlots.Count > 1) {
                    if (lobbySlots[1].localPlayerAuthority) {

                        lobbySlots[1].transform.position = gameManager.rightRacketSpawn.transform.position;
                    }
                }
                */


                ball = Instantiate(ballPrefab);
                NetworkServer.Spawn(ball);
            }
        }
    }
}
