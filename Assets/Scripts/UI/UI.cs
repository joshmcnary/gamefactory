﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public static UI access;

    public Text StateText;
    public Text StateTimer;

    public Text NetworkOutput;

    void Awake() {
        access = this;

        //WorldStateManager.access.StateChange += (state) => { SetStateText(state.ToString()); };
        //WorldStateManager.access.StateSecondsTick += (seconds) => { SetStateTimer(seconds); };
    }

    //
    public void SetStateText(string state) {
        if (state == "Transition") { return; }
        StateText.text = state;
    }

    //
    public void SetStateTimer(float seconds) {
        StateTimer.text = Mathf.Round(seconds).ToString();
    }

    public void AddNetworkOutputText(string text) {
        NetworkOutput.text = text + "\n" + NetworkOutput.text;
    }
}
