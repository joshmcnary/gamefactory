﻿using System;
using System.Collections.Generic;
using UnityEngine;
/*
public class GeoRegion {

    public event SendGeoRequest Request;

    // georegions must have access to a geomap to work properly since all nodes need to be known to select them properly
    private GeoMap geoMap = World.access.geoMap;

    //  
    public GeoRegionType regionType;

    // origin may be more than one node, ex: a 2x2 structure as the origin
    // circle - center node(s)
    // square - bottom left node
    public List<GeoNode> originNodes;    
    
    // circle - size is radius from the origin (center)
    // square - size is length from the origin (bottom left)       
    public int size;                          

    // these are the nodes that the whole region covers, function of georegion type and size
    // georegion nodes should be calculated by geomap functions
    // because the geomap is fully aware of all geonodes, it should handle the logic in calculating regions
    public List<GeoNode> regionNodes;
	
    // multiple origin nodes -- region nodes unknown
    public GeoRegion(GeoRegionType _regionType, List<GeoNode> _originNodes, int _size) {
        regionType = _regionType;
        originNodes = _originNodes;
        size = _size;
        regionNodes = new List<GeoNode>();

        CalculateRegionNodes();
    }

    // single origin node -- region nodes unknown
    public GeoRegion(GeoRegionType _regionType, GeoNode _originNode, int _size) {
        regionType = _regionType;
        originNodes = new List<GeoNode>() { _originNode };
        size = _size;
        regionNodes = new List<GeoNode>();

        CalculateRegionNodes();
    }

    // multiple origin nodes -- region nodes known
    public GeoRegion(GeoRegionType _regionType, List<GeoNode> _originNodes, int _size, List<GeoNode> _regionNodes) {
        regionType = _regionType;
        originNodes = _originNodes;
        size = _size;
        regionNodes = _regionNodes;
    }

    // single origin node -- region nodes unknown
    public GeoRegion(GeoRegionType _regionType, GeoNode _originNode, int _size, List<GeoNode> _regionNodes) {
        regionType = _regionType;
        originNodes = new List<GeoNode>() { _originNode };
        size = _size;
        regionNodes = _regionNodes;
    }

    // single node georegion shortcut
    public GeoRegion(GeoNode _node) {
        regionType = GeoRegionType.CornerSquare;
        originNodes = new List<GeoNode>() { _node };
        size = 1;
        regionNodes = originNodes;
    }

    private void SetGeoResponse(GeoResponse response) {

        Debug.Log(0);

    }

    // check whether a point is on the geomap
    private bool PointOnMap(GeoPoint point) {

        float x = point.x;
        float z = point.z;

        if (x >= 0 && x < geoMap.width && z >= 0 && z < geoMap.height) {
            return true;
        } else {
            return false;
        }

    }

    // determines what the region nodes should be based on region type, origin, and size. may include more parameters as this grows
    public void CalculateRegionNodes() {

        switch (regionType) {

            case GeoRegionType.Circle:

                for(int x = -size; x <= size; x++) {
                    for (int z = -size; z <= size; z++) {
                        int nodeX = originNodes[0].x + x;
                        int nodeZ = originNodes[0].z + z;

                        if (nodeX < 0 || nodeX >= geoMap.width) { continue; }
                        if (nodeZ < 0 || nodeZ >= geoMap.height) { continue; }

                        bool circleTest = Mathf.Pow(nodeX - originNodes[0].x, 2) + Mathf.Pow(nodeZ - originNodes[0].z, 2) <= Mathf.Pow(size, 2);
                        if (circleTest) {
                            regionNodes.Add(geoMap.Nodes[nodeX, nodeZ]);
                        }
                    }
                }



                // find top nodes
                List<Index> topZNodes = new List<Index>();
                for (int i = 0; i < regionNodes.Count; i++) {

                    int tempX = regionNodes[i].x;
                    int tempZ = regionNodes[i].z;

                    bool xExists = false;
                    for (int j = 0; j < topZNodes.Count; j++) {

                        if (topZNodes[j].x == tempX) {
                            xExists = true;
                            if (topZNodes[j].z < tempZ) {
                                topZNodes[j].z = tempZ;
                            }
                        }
                    }

                    if (!xExists) {
                        topZNodes.Add(new Index(tempX, tempZ));
                    }

                }

                // determine how far to extend circle upward
                int topZ = -1;
                for (int i = 0; i < originNodes.Count; i++) {

                    if (originNodes[i].z > topZ)
                        topZ = originNodes[i].z;
                }

                // extend the circle upward
                int zDiff = topZ - topZNodes[0].z;
                for (int i = 0; i < topZNodes.Count; i++) {
                    Index index = topZNodes[i];
                    for (int z = 1; z <= zDiff; z++) {
                        regionNodes.Add(geoMap.Nodes[index.x, index.z + z]);
                    }
                }


                // find right nodes
                List<Index> rightXNodes = new List<Index>();
                for (int i = 0; i < regionNodes.Count; i++) {

                    int tempX = regionNodes[i].x;
                    int tempZ = regionNodes[i].z;

                    bool zExists = false;
                    for (int j = 0; j < rightXNodes.Count; j++) {

                        if (rightXNodes[j].z == tempZ) {
                            zExists = true;
                            if (rightXNodes[j].x < tempX) {
                                rightXNodes[j].x = tempX;
                            }
                        }
                    }

                    if (!zExists) {
                        rightXNodes.Add(new Index(tempX, tempZ));
                    }

                }

                // determine how far to extend circle to the right
                int rightX = -1;
                for (int i = 0; i < originNodes.Count; i++) {

                    if (originNodes[i].x > rightX)
                        rightX = originNodes[i].x;
                }

                // extend the circle to the right
                int xDiff = rightX - originNodes[0].x;
                for (int i = 0; i < rightXNodes.Count; i++) {
                    Index index = rightXNodes[i];
                    for (int x = 1; x <= xDiff; x++) {
                        if (PointOnMap(new GeoPoint(index.x + x, index.z)))
                            regionNodes.Add(geoMap.Nodes[index.x + x, index.z]);
                    }
                }


                break;

            case GeoRegionType.CornerSquare:                

                // square will only ever contain 1 origin node
                GeoNode originNode = originNodes[0];

                for (int z = 0; z <= size; z++) {
                    for (int x = 0; x <= size; x++) {
                        int nodeX = originNode.x + x;
                        int nodeZ = originNode.z + z;

                        if (nodeX < 0 || nodeX >= geoMap.width) { continue; }
                        if (nodeZ < 0 || nodeZ >= geoMap.height) { continue; }
                        regionNodes.Add(geoMap.Nodes[nodeX, nodeZ]);
                    }
                }

                break;

        }
    }

    // checks whether this region has nodes that would be off the map
    // returns true if all region nodes are on the map
    public bool OnMapCheck() {

        switch (regionType) {

            case GeoRegionType.Circle:

                break;

            case GeoRegionType.CornerSquare:

                // square will only ever contain 1 origin node
                GeoNode originNode = originNodes[0];

                if (originNode.x + size >= geoMap.width) {
                    return false;
                }

                break;

        }

        return true;

    }

    // checks whether region nodes contain any blockables
    // returns true if any region nodes contain a blockable
    public bool BlockablesCheck() {

        foreach (GeoNode node in regionNodes) {
            if (node.blockableEntity != null) {
                return true;
            }
        }

        return false;

    }

    // checks how much of region in parameter fits inside region nodes
    // returns 0 if completely outside, 1 if completely inside, and a number in between if it is partially inside/outside
    public float RegionCheck(GeoRegion region) {

        if (region.regionNodes.Count == 0)
            return 0;

        float nodesInside = 0;

        foreach (GeoNode node in region.regionNodes) {

            int x = node.x;
            int z = node.z;

            switch (regionType) {

                case GeoRegionType.CornerSquare:

                    break;

                case GeoRegionType.Circle:

                    foreach (GeoNode originNode in originNodes) {

                        bool circleTest = Mathf.Pow(x - originNode.x, 2) + Mathf.Pow(z - originNode.z, 2) <= Mathf.Pow(size, 2);
                        if (circleTest) {
                            nodesInside++;
                            break;
                        }
                    }

                    break;

            }

        }

        return nodesInside / region.regionNodes.Count;
    }
}
*/
