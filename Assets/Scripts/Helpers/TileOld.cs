﻿using System;

/*
    MapNode has a MapPoint at its origin, which is determined by its indices and the mapnode size in MapData
*/


[Serializable]
public class TileOld {

    // events
    // public event System.Action BlockableEntityAdded;
    // public event System.Action BlockableEntityRemoved;

    // coordinates in the world
    public Point origin;           // this should be the center point of the node
    public int x { get { return (int)origin.x; } }
    public int z { get { return (int)origin.z; } }

    // used when a node needs to be referenced but has no data or is not part of the map
    public bool nullNode { get; set; }

    // height will be an integer, and will have a heightFactor applied to it to determine its y position
    private float height = 0;
    public float Height { get { return height; } set { height = value; } }

    //
    public TileType type;

    //[NonSerialized]
    public Entity blockableEntity = null;

    //Action EntityRemoved;

    public TileOld() { }       

    //
    public TileOld(int _x, int _z) {
        origin = new Point(_x, _z);
    }

    //
    public void AddBlockableEntity(Entity entity) {

        blockableEntity = entity;

        /*
        EntityRemoved = delegate {
            RemoveBlockableEntity();
            EntityRemoved = null;
        };
        entity.RemovedFromGame += EntityRemoved;
        */

    }

    //
    public void RemoveBlockableEntity() {

        blockableEntity = null;

    }

    //
    /*
    protected void OnBlockableEntityAdded(GeoNodeEventArgs args) {
        if (BlockableEntityAdded != null) {
            BlockableEntityAdded(this, args);
        }
    }

    //
    protected void OnBlockableEntityRemoved(GeoNodeEventArgs args) {
        if (BlockableEntityRemoved != null) {
            BlockableEntityRemoved(this, args);
        }
    }
    */

    public override string ToString() {
        return "X: " + x + ", Z: " + z;
    }

    public bool Equals(TileOld node) {
        return x == node.x && z == node.z;
    }
}