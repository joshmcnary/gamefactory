﻿using System.Diagnostics;
using System.Collections.Generic;

public static class Diagnostics {

    public static Stopwatch watch;

    static Diagnostics() {
        watch = new Stopwatch();
    }

    public static void Start() {
        watch.Reset();
        watch.Start();
    }

    public static void Stop() {
        watch.Stop();
        UnityEngine.Debug.Log(watch.ElapsedMilliseconds + " ms elapsed");
    }

    /*
    * Diagnostics.PerformanceTest( 
                        () => { },
                        () => { }
                    );
    *
    */
    public static void PerformanceTest(System.Action test1, System.Action test2, int testCount = 1000000) {

        int count = 0;
        int numberOfTimesToTest = testCount;

        Start();
        UnityEngine.Debug.Log("Test 1");
        while (count < numberOfTimesToTest) {

            test1();
            count++;

        }
        Stop();

        count = 0;

        Start();
        UnityEngine.Debug.Log("Test 2");
        while (count < numberOfTimesToTest) {

            test2();
            count++;

        }
        Stop();

    }

    public static void ListKeys(Dictionary<string, object> properties) {
        List<string> keyList = new List<string>(properties.Keys);
        foreach (string key in keyList) {
            UnityEngine.Debug.Log(key);
        }
    }
}
