﻿using System.Collections.Generic;
using UnityEngine;

// effects hold gameobjects / particle systems to be used by mapeffects
// effects can be constructed either using either origin points and a range, or by defining the effectpoints explicitly
public enum EffectObject { HoverBox, BuildBox, PowerBox }

[System.Serializable]
public class Effect {

    GameObject EffectRoot;

    //public List<Tile> originTiles { get; private set; }
    public List<TileOld> Tiles { get; private set; }

    public EffectType Type;

    GameObject EffectPrefab;
    List<GameObject> EffectObjects = new List<GameObject>();

    //
    public Effect(List<TileOld> tiles, EffectType type) {

        Tiles = tiles;
        Type = type;

        EffectRoot = new GameObject();
        EffectRoot.transform.parent = GridEffects.access.transform;
        EffectRoot.name = type.ToString();

        InitEffect();       

    }

    // this constructor is the newest way of constructing.. other ways should be deprecated
    /*
    public Effect(GeoRegion _region, EffectObject _effectObject) {

        region = _region;
        effectObject = _effectObject;

        originTiles = region.originNodes;
        range = region.size;
        effectPoints = region.regionNodes;

        layerEffect = EffectType.DisplayCircle;

        effectObjects = new List<GameObject>();


    }

    public Effect(List<GeoNode> _originPoints, int _range, EffectType _layerEffect, EffectObject _effectObject) {

        originTiles = _originPoints;
        effectPoints = new List<GeoNode>();
        layerEffect = _layerEffect;
        effectObject = _effectObject;
        range = _range;
        effectObjects = new List<GameObject>();
    }

    public Effect(List<GeoNode> _effectPoints, EffectType _layerEffect, EffectObject _effectObject) {
        
        effectPoints = _effectPoints;
        layerEffect = _layerEffect;
        effectObject = _effectObject;
        effectObjects = new List<GameObject>();

    }
    */

    //
    public void SetEffectObjects(List<GameObject> _effectObjects) {

        //effectObjects = _effectObjects;
    }

    //
    void InitEffect() {

        switch (Type) {

            case EffectType.Room:

                EffectPrefab = Resources.Load("Effects/RoomTile - Blue") as GameObject;

                break;

        }

        for (int i = 0; i < Tiles.Count; i++) {
            TileOld tile = Tiles[i];
            GameObject effectObject = GameObject.Instantiate(EffectPrefab, tile.origin.ToVector3() + new Vector3(0.5f, 0.01f, 0.5f), Quaternion.identity, EffectRoot.transform) as GameObject;
            EffectObjects.Add(effectObject);
        }

    }

    //
    public void Remove() {

        foreach (GameObject go in EffectObjects) {
            Object.Destroy(go);
        }

        EffectObjects.Clear();
    }

    /*
    public override string ToString() {

        string s = "";

        s += "Origin Points: ";
        foreach (GeoNode point in originTiles) {
            s += "( " + point.ToString() + " ) ";
        }

        s += " | Layer Effect: " + Type.ToString();

        s += " | Effect Range: " + range;

        return s;
    }
    */
}