﻿using System;
using UnityEngine;

public class Settings : ScriptableObject {

    public Type type;
    public new string name = "";

    public void Init<T>() {
        type = typeof(T);
        if (name == "") {
            name = type.ToString();
        }
    }
}
