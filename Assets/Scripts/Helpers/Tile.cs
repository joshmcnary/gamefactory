﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public enum TileType { None, Grass, Water }
public enum BlockingObject { None, Tree, Stone }

//
public struct Tile {

    public TileType type;
    public Point coords;
    public bool nullNode;
    public BlockingObject BlockingObject;

    public Point origin { get { return new Point(coords.ToVector3() + new Vector3(0.5f, 0, 0.5f)); } }

    public int BlockableEntityId;

    public Tile(int x, int z) {
        type = TileType.None;
        coords = new Point(x, z);
        nullNode = false;
        BlockableEntityId = 0;
        BlockingObject = BlockingObject.None;
    }

    //
    public void AddBlockableEntity(Entity entity) {
        NetworkIdentity netId = entity.GetComponent<NetworkIdentity>();
        BlockableEntityId = (int)netId.netId;
        GridData.access.SetTile((int)coords.x, (int)coords.z, this);
    }

    //
    public void RemoveBlockableEntity() {
        BlockableEntityId = 0;
        GridData.access.SetTile((int)coords.x, (int)coords.z, this);
    }

    //
    public void SetBlockingObject(BlockingObject obj) {

        //
        if (obj == BlockingObject) { return; }

        // remove current object
        int index = (int)(coords.z * GridData.access.Width + coords.x);
        switch (BlockingObject) {
            case BlockingObject.Tree: EntityManager.access.TreePool[index].transform.position = GridData.offMapPoint.ToVector3();                
                break;
            case BlockingObject.Stone:
                //EntityManager.access.StonesPool[index].transform.position = GridData.offMapPoint.ToVector3();
                break;
        }

        BlockingObject = obj;

        // add current object
        switch (BlockingObject) {
            case BlockingObject.Tree:
                EntityManager.access.TreePool[index].transform.position = new Vector3(coords.x, 0, coords.z);
                break;
            case BlockingObject.Stone:
                //EntityManager.access.StonesPool[index].transform.position = new Vector3(coords.x, 0, coords.z);
                break;
        }
        
        GridData.access.SetTile((int)coords.x, (int)coords.z, this);
    }

    public override string ToString() {
        return coords.ToString() + " Type: " + type.ToString() + " BEid: " + BlockableEntityId;
    }
}
