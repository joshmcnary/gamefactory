﻿/*
    GeoPoint basically a very simple Vector3, only holds positional data

    x is the horizontal axis, z is the vertical axis, and y is the height axis    
*/

using UnityEngine;

[System.Serializable]
public struct Point {

    public float x;
    public float y;
    public float z;

    public Point(float _x, float _y, float _z) {
        x = _x;
        y = _y;
        z = _z;
    }

    public Point(int _x, int _y, int _z) {
        x = _x;
        y = _y;
        z = _z;
    }

    public Point(int _x, int _z) {
        x = _x;
        y = 0;
        z = _z;
    }

    public Point(float _x, float _z) {
        x = _x;
        y = 0;
        z = _z;
    }

    public Point(Vector3 point) {
        x = point.x;
        y = point.y;
        z = point.z;
    }

    public Point AddPoint(Point pointToAdd) {
        return new Point(ToVector3() + pointToAdd.ToVector3());
    }

    public Vector3 ToVector3() {
        return new Vector3(x, y, z);
    }

    public override string ToString() {
        return "{ X: " + x + " , Y: " + y + " , Z: " + z + " }";
    }

    //
    public bool Equals(Point point) {

        bool equals = false;

        if (Mathf.Approximately(point.x, x) && Mathf.Approximately(point.y, y) && Mathf.Approximately(point.z, z)) {
            equals = true;
        }

        return equals;

    }

    //
    public bool Equals(Vector3 point) {
        return Equals(new Point(point));
    }
}
