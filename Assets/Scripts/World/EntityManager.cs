﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Mirror;

/*
  
   EntityManager keeps track of all entities in game and allows manipulation at a high level
  
   There should be a lifecycle for an entity of Default -> Created -> Added -> Removed -> Destroy
   This allows certain assumptions to be made based on where it is at in the flow and help prevent bugs with transitioning between states

*/

public enum EntityClass { Structure, Human, Machine, Obstacle, Path }

public enum EntityType {

    // this is needed to check entity settings at time of default entity creation
    // this ensures entitype appropriate for the entity being created
    None, 

    Castle,
    Cannon,
    Wall = 50,
    WallPlus,

}

// this allows a distinction for which components are considered active
public enum EntityState {
    Default,            // entity is the only component attached to the gameobject and should have default entity settings
    Basic,              // entity has been instantiated and all components added, but only entity component active
    Full,               // all components active
    Destroyed           // will be destroyed on the next update
}

//
public class EntityManager : NetworkBehaviour {

    public static EntityManager access;

    public event Action<Entity> EntityCreated;
    public event Action<Entity> EntityAddedToGame;
    public event Action<Entity> EntityRemovedFromGame;

    public static EntityType[] AllEntityTypes;

    // holds a default prefab for all entities
    public Dictionary<EntityType, Entity> DefaultEntities { get; set; }

    // list of unprocessed entity requests
    List<EntityRequest> entityRequests = new List<EntityRequest>();

    // list of all entities currently in world
    public int entityCount;
    public List<Entity> entities = new List<Entity>();
    public Dictionary<int, Entity> entitiesNew = new Dictionary<int, Entity>();
    //public List<Entity> humans = new List<Entity>();
    //public List<StoreHuman> Housing = new List<StoreHuman>();

    public GameObject entitiesRoot;
    public GameObject defaultEntitiesRoot;
    //public static GameObject unitsObject = GameObject.Find("Units");
    //public static GameObject humansObject = GameObject.Find("Humans");
    //public static GameObject structuresObject = GameObject.Find("Structures");
    //public static GameObject pathsObject = GameObject.Find("Paths");
    int count;

    public LocalObject[] TreePool;
    public LocalObject[] StonesPool;

    //
    public void Init() {

        // populate AllEntityTypes
        AllEntityTypes = (EntityType[])Enum.GetValues(typeof(EntityType));

        defaultEntitiesRoot = transform.GetChild(0).gameObject;

        LoadDefaultEntities();

        TreePool = GameObject.Find("Trees").GetComponentsInChildren<LocalObject>();
        StonesPool = GameObject.Find("Stones").GetComponentsInChildren<LocalObject>();

        if (World.access.isClientOnly) {
            InitBlockingObjectsOnTiles();
        }
    }

    //
    /*
    public void SetTileEntities() {

        int count = 0;
        for (int z = 0; z < GridData.access.Width; z++) {
            for (int x = 0; x < GridData.access.Height; x++) {
                TreePool[count].transform.position = new Vector3(x, 0, z);
                count++;
            }
        }

    }
    */

    //
    public void InitSettings(WorldSettings settings) {

        /*
        if (settings.GaeanGenerationRate != 0) {
            HumanGenerationRate = settings.GaeanGenerationRate;            
        } else {
            HumanGenerationRate = 99999;
        }

        MaxGaeans = settings.MaxGaeans;
        HumanWinCondition = settings.GaeanWinCondition;
        UI.access.Conditions.SetGoal(HumanWinCondition);
        */
    }

    public void Run() {
        
       entitiesNew = new Dictionary<int, Entity>();

       CreateStartingWalls();

       InitBlockingObjectsOnTiles();

    }

    //
    void CreateStartingWalls() {

        /*
        int startX = 6;
        int startZ = 6;
        int endX = 10, endZ = 10;
        for (int z = startZ; z <= endZ; z++) {
            for (int x = startX; x <= endX; x++) {
                if (x == startX || z == startZ || x == endX || z == endZ) {
                    AddNewEntityToGame(EntityType.Wall, new Point(x, z));
                }
            }
        }
        */

        int count = 0;
        for (int z = 0; z < GridData.access.Size; z++) {
            for (int x = 0; x < GridData.access.Size; x++) {
                bool createEntity = UnityEngine.Random.Range(0, 2) != 0;
                if (createEntity) {
                    //AddNewEntityToGame(EntityType.Wall, new Point(x, z));
                    //count++;
                }
            }
        }

    }

    //
    public void InitBlockingObjectsOnTiles() {
        
        for (int z = 0; z < GridData.access.Size; z++) {
            for (int x = 0; x < GridData.access.Size; x++) {
                SetBlockingObjectOnTile(x, z, GridData.access.GetTileNew(x, z).BlockingObject);                
            }
        }
    }

    //
    public void SetBlockingObjectOnTile(int x, int z, BlockingObject obj) { 

        int index = z * GridData.access.Width + x;
        switch (obj) {
            case BlockingObject.None:
                TreePool[index].transform.position = GridData.offMapPoint.ToVector3();
                //StonesPool[index].transform.position = GridData.offMapPoint.ToVector3();
                break;
            case BlockingObject.Tree:
                TreePool[index].transform.position = new Vector3(x, 0, z);
                //StonesPool[index].transform.position = GridData.offMapPoint.ToVector3();
                break;
            case BlockingObject.Stone:
                TreePool[index].transform.position = GridData.offMapPoint.ToVector3();
                //StonesPool[index].transform.position = new Vector3(x, 0, z);
                break;
        }
    }

    //
    /*
    public void FixedUpdate() {

        if (HumanGenerationRate == 0) { return; }

        humansTimer += Time.fixedDeltaTime;
        if (humansTimer >= HumanGenerationRate) {

            // setting for controlling max number of gaeans to spawn
            if (humans.Count >= MaxGaeans) {
                return;
            }

            //ForTesting = true;
            humansTimer = 0;

            // determine if a human should be created
            StoreHuman[] totalHousing = Housing.ToArray();
            StoreHuman currentHousing = null;
            int totalCapacity = 0;
            int availableCapacity = 0;
            for (int i = 0; i < totalHousing.Length; i++) {
                StoreHuman housing = totalHousing[i];
                if (housing.entity.EntityState != EntityState.AllModules) { continue; }
                totalCapacity += housing.Settings.capacity;
                availableCapacity += housing.Settings.capacity - housing.AssignedHumans.Count;
                if (availableCapacity > 0 && currentHousing == null) {
                    currentHousing = housing;
                }
            }


            // full capacity warning
            if (availableCapacity == 0) {
                UI.access.warningDisplay.AddWarning("There is no housing available for new colonists.");
                return;
            }

            // create human
            UI.access.warningDisplay.AddWarning("A new colonist is joining your settlement.");

            Entity human = CreateEntity(EntityType.Human, PlayerManager.access.players[1].locationTile);
            human.GetComponent<Worker>().Paused = true;

            if (NoGaeanConsumption) {
                human.GetComponent<ConsumeResource>().enabled = false;
            }

            MoveRoute newHumanRoute = new MoveRoute();
            Entity hq = currentHousing.entity;

            if (hq != null) {

                GeoNode gateNode = hq.StandardModule.Blockable.gaeanGates[0];
                ActionGroup enterMap = new ActionGroup();
                enterMap.AddSyncAction(() => {
                    hq.GetComponent<StoreHuman>().AddHuman(human);
                });
                newHumanRoute.AddRouteSegment(gateNode, enterMap);

                GeoNode stopNode = hq.StandardModule.Blockable.humanStops[0];
                ActionGroup enterHq = new ActionGroup();
                enterHq.AddSyncAction(() => {
                    human.GetComponent<Worker>().Paused = false;
                });
                newHumanRoute.AddRouteSegment(stopNode, enterHq);

                human.StandardModule.Movable.SetCurrentRoute(newHumanRoute);

            }

            // conditions ui update
            UI.access.Conditions.SetHousing(totalCapacity);
            UI.access.Conditions.SetGaeans(humans.Count);

            // win condition check
            if (humans.Count == HumanWinCondition) {
                WorldStateManager.access.Win();
                return;
            }
        }

    }
    */

    // 
    public void Update() {

        foreach (EntityRequest request in entityRequests) {
            //ProcessEntityRequest(request);
        }
        entityRequests.Clear();

    }

    //
    public void Reset() {
        
        for (int i = 0; i < entities.Count; i++) {
            entities[i].BeingDestroyed = true;
            Destroy(entities[i].gameObject);
        }

        entityCount = 0;
        entities = new List<Entity>();
        

    }
    
    // this loads a default prefab for each entitytype
    // this makes it so that all entities created afterward can be instantiated from this default prefab rather than resources.load
    void LoadDefaultEntities() {

        // create dict
        DefaultEntities = new Dictionary<EntityType, Entity>();

        // get all entity settings from resources
        object[] entityPrefabs = Resources.LoadAll("Entities");

        // instantiate a gameobject and add an entity component
        for (int i = 0; i < entityPrefabs.Length; i++) {
            //CreateDefaultEntity((GameObject)entityPrefabs[i]);
        }

        // FOR DEBUGGING -- use this to identify any possible resource loading issues
        /*
        EntityType[] allTypes = (EntityType[])Enum.GetValues(typeof(EntityType));
        for (int i = 0; i < allTypes.Length; i++) {
            Debug.Log(allTypes[i]);
            string entityString = allTypes[i].ToString().Replace("_", "");
            Resources.Load("Entities/" + entityString);
        }
        */

    }

    // creates a default entity -- prefab with minimum loading and only active module is entity
    // this would only happen when the game initially loads
    void CreateDefaultEntity(GameObject prefab) {        

        if (prefab.name == "Entity") { return; }

        EntityType entityType = prefab.GetComponent<Entity>().entityType;
        if (entityType == EntityType.None) {
            Debug.Log("No entity type selected for " + prefab.name);
            return;
        }

        // check if this entitytype already exists in dict
        if (DefaultEntities.ContainsKey(entityType)) {
            Debug.Log("Entity type selected in " + prefab.name + " already exists in default entities");
            return;
        }

        // instantiate gameobject
        GameObject go = Instantiate(prefab);
        go.transform.position = GridData.offMapPoint.ToVector3();

        // load default entity
        Entity defaultEntity = go.GetComponent<Entity>();
        defaultEntity.SetState(EntityState.Default);

        // organize default entities in hierarchy
        defaultEntity.transform.parent = defaultEntitiesRoot.transform;

        // add to default entities dict
        DefaultEntities.Add(entityType, defaultEntity);

    }

    // this is used primarily as a way to break the entity creation process into smaller steps
    // this would be primarily used when wanting to manipulate entities outside of the normal game
    public Entity CreateEntity(EntityType entityType) {

        Entity createdEntity = null;

        if (DefaultEntities.ContainsKey(entityType)) {
            createdEntity = Instantiate(DefaultEntities[entityType]);
            createdEntity.SetState(EntityState.Basic);
            createdEntity.id = entityCount;
            entityCount++;
            createdEntity.name = createdEntity.name + " " + createdEntity.id;
            entities.Add(createdEntity);
        } else {
            Debug.Log("No settings were created for " + entityType.ToString());
        }

        return createdEntity;

    }

    //
    public Entity CreateEntityTemp(EntityType entityType) {

        GameObject spawnObject = null;
        List<GameObject> spawnPrefabs = NetworkManager.singleton.spawnPrefabs;
        switch (entityType) {
            case EntityType.Cannon: spawnObject = spawnPrefabs[0]; break;
            case EntityType.Castle: spawnObject = spawnPrefabs[1]; break;
            case EntityType.Wall: spawnObject = spawnPrefabs[2]; break;
        }

        GameObject c = Instantiate(spawnObject);
        Entity createdEntity = c.GetComponent<Entity>();
        createdEntity.SetState(EntityState.Basic);
        //createdEntity.id = entityCount;
        entityCount++;
        createdEntity.name = createdEntity.name + " " + createdEntity.id;
        entities.Add(createdEntity);

        NetworkServer.Spawn(c);

        createdEntity.id = (int)c.GetComponent<NetworkIdentity>().netId;
        entitiesNew.Add(createdEntity.id, createdEntity);

        return createdEntity;

    }

    //
    public Entity CreateEntityTemp(EntityType entityType, Tile tile) {

        GameObject spawnObject = null;
        List<GameObject> spawnPrefabs = NetworkManager.singleton.spawnPrefabs;
        switch (entityType) {
            case EntityType.Cannon: spawnObject = spawnPrefabs[0]; break;
            case EntityType.Castle: spawnObject = spawnPrefabs[1]; break;
            case EntityType.Wall: spawnObject = spawnPrefabs[2]; break;
        }

        GameObject c = Instantiate(spawnObject);
        Entity createdEntity = c.GetComponent<Entity>();
        createdEntity.SetState(EntityState.Basic);
        createdEntity.id = entityCount;
        entityCount++;
        createdEntity.name = createdEntity.name + " " + createdEntity.id;
        entities.Add(createdEntity);

        NetworkServer.Spawn(c);

        c.transform.position = tile.coords.ToVector3();

        return createdEntity;

    }

    //
    public void AddEntityToGame(Entity entity) {
        entity.AddToGame();
        OnEntityAddedToGame(entity);
    }

    // add entity to class-specific lists
    /*
    public void AddEntityToLists(Entity createdEntity) {

        entities.Add(createdEntity);
        if (createdEntity.entityClass == EntityClass.Human) {
            humans.Add(createdEntity);
        }

        // add to component specific lists
        StoreHuman housing = createdEntity.GetComponent<StoreHuman>();
        if (housing != null) {
            if (housing.Settings.storeHumanType == StoreHumanType.Residential) {
                Housing.Add(housing);
            }
        };

    }

    //
    public void RemoveEntityFromLists(Entity removedEntity) {

        entities.Remove(removedEntity);
        if (removedEntity.entityClass == EntityClass.Human) {
            humans.Remove(removedEntity);
        }

        // add to component specific lists
        StoreHuman housing = removedEntity.GetComponent<StoreHuman>();
        if (housing != null) {
            if (housing.Settings.storeHumanType == StoreHumanType.Residential) {
                Housing.Remove(housing);
            }
        };
    }

    //
    public void InitEntityEvents(Entity entity) {

        if (entity.modules != null) {
            for (int i = 0; i < entity.modules.Length; i++) {
                Module module = entity.modules[i];
                module.GeoRequestEvent += delegate (object o, GeoRequestEventArgs args) {
                    GeoRequest request = args.request;
                    request.response = GeoMap.access.ProcessGeoRequest(request);
                    module.ProcessGeoResponse(request);
                };

                module.SendInitialSystemRequests();
                
            }
        }

        // entitycreated event
        EntityManagerEventArgs _args = new EntityManagerEventArgs();
        _args.entity = entity;
        OnEntityCreated(_args);
    }
    */

    // this creates an entity and adds it to the game and map
    public Entity AddNewEntityToGame(EntityType entityType, Point origin, float rotation = 0) {

        Entity createdEntity = CreateEntityTemp(entityType);

        // validate adding to map
        bool validMove = true; // createdEntity.Blockable.ValidateMove(origin);
        if (validMove) {
            AddEntityToGame(createdEntity);
            createdEntity.AddToMap(origin, rotation);
        }
        else {
            DestroyEntity(createdEntity);
            Debug.Log("Trying to create " + entityType + " but it is an invalid move");
            return null;
        }

        return createdEntity;
    }

    // create using a single node
    /*
    public Entity CreateEntity(EntityType entityType, MapNode nodeToBuildOn, float rotation = 0, MapPoint offset = null) {
        return CreateEntity(entityType, new List<MapNode>() { nodeToBuildOn }, rotation, offset);
    }

    // shortcut for creating entities
    public Entity CreateEntity(EntityType entityType, MapPoint point, float rotation = 0, MapPoint offset = null) {
        return CreateEntity(entityType, new List<MapNode>() { World.access.MapData.GetMapNode(new Vector3(point.x, 0, point.z)) }, rotation, offset);
    }

    // shortcut for creating entities
    public Entity CreateEntity(EntityType entityType, int x, int z, float rotation = 0, MapPoint offset = null) {
        return CreateEntity(entityType, new List<MapNode>() { World.access.MapData.GetMapNode(new Vector3(x, 0, z)) }, rotation, offset);
    }
    */

    //
    /*
    public void AddEntityRequest(EntityRequest request) {
        entityRequests.Add(request);
    }

    //
    public void ProcessEntityRequest(EntityRequest request) {

        EntityRequestType requestType = request.requestType;

        switch (requestType) {

            case EntityRequestType.Create:

                EntityType entityType = request.entityType;
                GeoNode originNode = request.originNode;
                float rotationAngle = request.RotationAngle;

                Entity createdEntity = CreateEntity(entityType, originNode, rotationAngle);

                request.Callback(createdEntity);

                break;

            case EntityRequestType.Destroy:

                DestroyEntity(request.entity);

                break;

        }

    }
    */

    // this removes the entity from the game and interacting with the entities still remaining
    // the entity will still exist though, and will remain until destroyed
    public void RemoveEntityFromGame(Entity entity) {

        entity.RemoveFromGame();
        //RemoveEntityFromLists(entity);

        /*
        for (int i = 0; i < entity.modules.Length; i++) {
            entity.modules[i].RemoveFromGame();
        }
        */

        //OnEntityRemovedFromGame(new EntityManagerEventArgs());

    }

    // destroy entities
    public void DestroyEntity(Entity entity) {

        // entitydestroyed event
        //EntityManagerEventArgs args = new EntityManagerEventArgs();
        //OnEntityDestroyed(args);

        // remove any references from modules
        /*
        if (entity.entityClass == EntityClass.Human) {
            if (entity.GetComponent<Worker>().CurrentHousing != null) {
                entity.GetComponent<Worker>().CurrentHousing.DestroyHuman(entity);
            }

        } else if (entity.entityClass == EntityClass.Structure) {
            //RemoveEntityFromLists(entity);
        }
        */

        entity.Destroy();

        //
        entities.Remove(entity);
        entitiesNew.Remove(entity.id);
        Destroy(entity.gameObject);

    }

    //
    public void RemoveAndDestroyEntity(Entity entity) {
        RemoveEntityFromGame(entity);
        DestroyEntity(entity);
    }

    // used for displaying entities in the Unity UI
    public static void SetEntityParentObject(Entity e) {

        /*
        //Setting parent object
        Transform t = null;
        switch (e.entityType) {
            case EntityType.Human:
                t = humansObject.transform;
                break;
            case EntityType.Structure:
                t = structuresObject.transform;
                break;
            case EntityType.Unit:
                t = unitsObject.transform;
                break;
            case EntityType.Path:
                t = pathsObject.transform;
                break;
        }
        */
        // e.go.transform.parent = t;
    }

    // return entities by class -- structure, machine, human
    /*
    public List<Entity> GetEntitiesByClass(EntityClass entityClass) {

        List<Entity> _entities = new List<Entity>();

        for (int i = 0; i < entities.Count; i++) {
            Entity tempEntity = entities[i];
            EntityClass tempEntityClass = tempEntity.entityClass;
            if (entityClass == tempEntityClass) {
                _entities.Add(tempEntity);
            }
        }

        return _entities;

    }
    */

    //
    public Entity GetEntityById(int id) {
        return entitiesNew[id];
    }

    //
    public List<Entity> GetEntitiesByType(EntityType entityType) {

        List<Entity> _entities = new List<Entity>();

        for (int i = 0; i < entities.Count; i++) {
            Entity tempEntity = entities[i];
            EntityType tempEntityType = tempEntity.entityType;
            if (entityType == tempEntityType) {
                _entities.Add(tempEntity);
            }
        }

        return _entities;

    }

    //
    public void FixListsOnLoad() {

        foreach (Entity e in entities) {

            // e.Load(); //Re-establishes Gameobject connection
            //SetEntityParentObject(e); //fixes parent objects of entity
            /*
            switch (e.entityType) { //fixes lists

                //case EntityType.Human: humans.Add((Human)e); break;
                //case EntityType.Structure: structures.Add((Structure)e); break;
                //case EntityType.Unit: units.Add((Unit)e); break;
                //case EntityType.Path: paths.Add((Path)e); break;
            }
            */
        }
    }

    //saves data
    // public static void SaveData() { foreach(Entity e in entities) { e.Save(); } }

    //deletes entities from scene so they can be loaded
    public void ClearEntities() {

        for (int i = entities.Count - 1; i > -1; i--) {
            //DestroyEntity(entities[i]);
        }
    }

    //
    public List<EntityData> ToData() {

        List<EntityData> entitiesData = new List<EntityData>();
        for (int i = 0; i < entities.Count; i++) {
            EntityData data = entities[i].ToData();
            entitiesData.Add(data);
        }

        return entitiesData;
    }

    //
    public void LoadData(WorldData data) {

        /*

        ClearEntities();

        List<EntityData> entitiesData = data.Entities;

        for (int i = 0; i < entitiesData.Count; i++) {
            EntityData entityData = entitiesData[i];
            CreateEntity(entityData.entityType, entityData.point);
        }
        */

    }

    //
    protected void OnEntityCreated(Entity entity) {
        EntityCreated?.Invoke(entity);
    }

    //
    protected void OnEntityAddedToGame(Entity entity) {
        EntityAddedToGame?.Invoke(entity);
    }

    //
    protected void OnEntityRemoved(Entity entity) {
        EntityRemovedFromGame?.Invoke(entity);
    }

    // HUMANS MANIPULATION

}

[Serializable]
public enum EntityRequestType { Create, Destroy };

[Serializable]
public delegate void EntityResponse(Entity entity);

// this class defines making requests to create/destroy entities
// the purpose of this is because entities (gameobjects) can only be created/destroyed on the main thread while actions may be executing on a different thread
// by creating and destroying entities via request, we can be sure that it will be done inside the update loop
[Serializable]
public class EntityRequest {

    public EntityRequestType requestType;

    // create variables
    public EntityType entityType;
    public TileOld originNode;

    // destroy variables
    public Entity entity;

    // used to allow a callback once request is processed
    public EntityResponse Callback;

    // this can be used to rotate the entity after its created
    public float RotationAngle = 0;

    // create request
    public EntityRequest(EntityType _entityType, TileOld _originNode, EntityResponse callback = null, float rotationAngle = 0) {

        requestType = EntityRequestType.Create;

        entityType = _entityType;
        originNode = _originNode;
        Callback = callback;
        RotationAngle = rotationAngle;

    }

    // destroy request
    public EntityRequest(Entity _entity) {

        requestType = EntityRequestType.Destroy;

        entity = _entity;

    }

    //
    public override string ToString() {

        return "Request Type: " + requestType.ToString() + ", Entity Type: " + entityType;
    }

}
