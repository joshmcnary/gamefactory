﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

// World is a super manager class used to bootstrap the game world and its managers in a controlled way
// On Awake, all managers use their Init() method, which should be used for functions that only need to happen once per world instance
// On Start() and Reset(), the LoadingSequence() method is run, which resets managers to their default state, loads any settings, then uses Run() after all managers are loaded
public class World : NetworkBehaviour {

    public static World access;

    [Header("World Settings")]
    public WorldSettings settings;                              // original scriptable object asset set in inspector
    public WorldSettings Settings { get; private set; }         // instantiation of scriptable object asset

    // Managers divide important game data and logic into meaningful groups
    [Header("Managers")]
    //public PlayerManager playerManager;             // manages players joining/leaving game
    //public ResourceManager resourceManager;         // manages resources for each player
    public GridData GridData;                           // contains position-specific data for the world
    public GridView GridView;                           // visual represation of mapdata, detects user interaction with map
    public GridEffects GridEffects;                     // added effects that happen in the world
    public EntityManager EntityManager;               // manages all entities in game
    public WorldStateManager State;                   // manages states for the game
    //public UtilitiesManager Util;                   // holds utility classes    

    // camera
    [Header("Camera")]
    public CameraController Camera;

    void Awake() {
        access = this;
    }

    //
    public override void OnStartServer() {
        base.OnStartServer();
        InitSequence();
        StartSequence();
    }

    //
    public override void OnStartClient() {
        base.OnStartClient();
        if (isClientOnly) {
            UI.access.AddNetworkOutputText("world started");
            InitSequence();
        }
    }

    // the equivalent of the Awake command for the engine should be Init.. 
    // the awake call should be used to set up references and listeners, functions that only need to happen once for a world instance
    public void InitSequence() {

        // setup static reference for world and managers
        SetStaticReferences();

        // initialize all managers
        InitManagers();

        // initialize event listeners
        InitListeners();
        
        // should convert initlisteners to this method so that only events that need to be processed by the ui exist in this class
        // InitUIListeners();
    }

    //
    public void Reset() {
        //LoadingSequence();

        //UI.access.SettingsMenu.Hide();
    }

    // this method is to ensure both have the same methodology at all times
    public void StartSequence() {

        // this puts managers back to their default states
        ResetManagers();

        // this will load the world settings
        LoadSettings();

        // this will be for any method that needs to ensure that settings are loaded
        Run();
    }

    // because monobehaviours have no constructor, we must set access reference inside a method when the world instance awakes
    void SetStaticReferences() {

        access = this;

        //PlayerManager.access = playerManager;
        //ResourceManager.access = resourceManager;
        GridData.access = GridData;
        GridView.access = GridView;
        GridEffects.access = GridEffects;
        EntityManager.access = EntityManager;
        WorldStateManager.access = State;
        //UtilitiesManager.access = Util;
        

    }

    // this should handle all the functions that only need to occur once for a manager
    void InitManagers() {

        GridData.Init();
        GridView.Init();
        EntityManager.Init();
        
        //resourceManager.Init();
        //MapEffects.Init();
        State.Init();
        //Util.Init();
        

    }

    //
    void ResetManagers() {

        State.Reset();
        EntityManager.Reset();

        /* /
        WorldUI.access.Reset();

        resourceManager.Reset();
        */

    }

    // instantiates a copy of the currently loaded world settings, then passes those settings to the managers
    // this should occur after managers Reset(), and before they are Run()
    void LoadSettings() {

        if (settings != null) {
            Settings = Instantiate(settings);
        }

        GridData.InitSettings(Settings);
        GridView.LoadSettings(Settings);
        GridEffects.InitSettings();
        EntityManager.InitSettings(Settings);
        //State.LoadSettings(Settings);
        

    }

    // Use this for initialization
    void Run() {

        GridData.Run();
        GridView.Run();
        GridEffects.Run();
        EntityManager.Run();
        State.Run();


        /* /
        Util.TutorialManager.Init();        // this is a sloppy place to put this, but I'm putting it right here for now because it needs the tutorial container to init first
        Util.TutorialManager.Run();
        */

        // this should be the last thing to happen after everything in the world is fully loaded and ready
        //Settings.LastRunScript();

    }

    //
    protected virtual void InitListeners() { }

    //
    public WorldData ToData() {
        return new WorldData(this);
    }

    //
    public void LoadData(WorldData data) {
        
        /* /
        geoMap.LoadData(data);
        entityManager.LoadData(data);
        */

    }
}

