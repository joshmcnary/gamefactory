﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EffectType { None, Room }

public class GridEffects : MonoBehaviour {

    public static GridEffects access;

    // 
    TileOld[,] Tiles { get { return World.access.GridData.TilesOld; } }

    //
    List<Effect> Effects = new List<Effect>();

    // geomap dimensions
    private int MapWidth { get { return World.access.GridData.Width; } }
    private int MapHeight { get { return World.access.GridData.Height; } }

    [Header("Node Highlight")]
    public bool showHoverBoxes = false;
    public GeoRegionType hoverBoxMode = GeoRegionType.CornerSquare;
    public int hoverBoxLength = 1;
    List<GameObject> hoverBoxes;
    GameObject hoverBox;

    public Entity Wall;
    public Entity Cannon;
    public GameObject RoomTile;

    //[Header("Effects")]
    /*
    public List<Effect> effects { get; set; }
    public List<Effect> stateEffects { get; set; }
    private Dictionary<EffectObject, GameObject> effectsDictionary;

    [Header("Resource Markers")]
    public ResourceType activeResource = ResourceType.Default;
    float updateTimer = 1f;
    float updateTimeCounter = 0;

    [Header("Effect Models")]
    public GameObject humanGatePrefab;
    public GameObject humanGate { get; set; }
    public GameObject machineGatePrefab;
    public GameObject machineGate { get; set; }
    public GameObject goodsPrefab;
    public GameObject goods { get; set; }

    private GameObject[,] waterResourceMarkers;
    private GameObject[,] ironResourceMarkers;

    public ParticleSystem MeteorShower;
    bool StartMeteorShowerTimer = true;
    float MeteorShowerTimer;
    float MeteorShowerInterval = 30;
    float MeteorShowerDuration = 10;

    public BoxHighlight BoxHighlight;
    */

    public void Init() {

        // mouse hover boxes
        hoverBoxes = new List<GameObject>();

        // mouse hover box        
        hoverBox = GameObject.CreatePrimitive(PrimitiveType.Cube);
        hoverBox.name = "hoverBox";
        hoverBox.transform.localScale = new Vector3(1f, 0.01f, 1f);
        hoverBox.transform.parent = transform;
        hoverBox.transform.position = GridData.offMapPoint.ToVector3();
        hoverBox.GetComponent<MeshRenderer>().material = Resources.Load("Materials/NodeHighlight") as Material;
        hoverBox.GetComponent<BoxCollider>().enabled = false;



        WorldStateManager.access.UpdateHighlightedMapNode += delegate (TileOld node) { SetHighlightedTile(node); };
        WorldStateManager.access.UpdateEntityToBuild += delegate (EntityType type) {
            Wall = EntityManager.access.CreateEntity(type);
            SetHighlightedTile(WorldStateManager.access.highlightedTile);
            //Wall.Move(WorldStateManager.access.highlightedMapNode.origin);
        };
        WorldStateManager.access.StateChange += delegate (GameState state) { ProcessStateChange(state); };
        WorldStateManager.access.RoomAdded += delegate (Room room) {
            AddEffect(EffectType.Room, room.Tiles);
        };
        WorldStateManager.access.RoomsUpdated += delegate (List<Room> rooms) {
            for (int i = 0; i < Effects.Count; i++) {
                var effect = Effects[i];
                if (effect.Type != EffectType.Room) { continue; }

                Room room = rooms.Find((r) => { return r.Tiles.Contains(effect.Tiles[0]); });
                if (room == null) { RemoveEffect(effect); i--; }
            }
        };




        // human and machne gates
        /*
        humanGate = Instantiate(humanGatePrefab);
        humanGate.transform.parent = transform;
        humanGate.transform.position = GeoMap.offMapPoint.ToVector3();
        humanGate.AddComponent<Entity>();

        machineGate = Instantiate(machineGatePrefab);
        machineGate.transform.parent = transform;
        machineGate.transform.position = GeoMap.offMapPoint.ToVector3();
        machineGate.AddComponent<Entity>();

        // goods
        goods = Instantiate(goodsPrefab, transform);
        goods.transform.position = GeoMap.offMapPoint.ToVector3();

        // effects
        effects = new List<Effect>();
        stateEffects = new List<Effect>();

        // effects dictionary
        effectsDictionary = new Dictionary<EffectObject, GameObject>();
        effectsDictionary.Add(EffectObject.HoverBox, hoverBox);

        // resource markers
        /*
        CreateResourceMarkers(World.access.geoMap.nodes);
        UpdateResourceMarkers(World.access.geoMap.nodes);
        HideResourceMarkers(ResourceType.Water);
        HideResourceMarkers(ResourceType.Iron);
        */
    }

    public void InitSettings() {

        //BoxHighlight.Init();

    }

    //
    public void Run() {

        // wall for build
        Cannon = EntityManager.access.CreateEntity(EntityType.Cannon);

    }

    // Update is called once per frame
    void Update() {

        /*
        WorldStateManager stateManager = World.access.stateManager;

        

        // hover boxes
        if (showHoverBoxes) {
            if (stateManager.highlightMode == HighlightMode.GeoView && !stateManager.highlightedGeoNode.dummy && !stateManager.isBuilding) {

                // @TODO: this should be changed, very inefficient to call this every update
                GeoRegion hoverRegion = World.access.geoMap.GetGeoRegion(hoverBoxMode, stateManager.highlightedGeoNode, hoverBoxLength);
                List<GeoNode> geoNodes = hoverRegion.regionNodes;

                if (hoverBoxes.Count == geoNodes.Count) {

                    // move hover boxes
                    for (int i = 0; i < hoverBoxes.Count; i++) {
                        GameObject hoverBox = hoverBoxes[i];
                        GeoNode node = geoNodes[i];
                        hoverBox.transform.position = new Vector3(node.x + 0.5f, node.CenterPoint.y + 0.02f, node.z + 0.5f);
                    }

                } else {

                    // clear hover boxes
                    foreach(GameObject hBox in hoverBoxes) {
                        Destroy(hBox);
                    }
                    hoverBoxes.Clear();

                    // create hover tiles 
                    hoverBox.SetActive(true);
                    for (int i = 0; i < geoNodes.Count; i++) {
                        GeoNode node = geoNodes[i];
                        GameObject hBox = Instantiate(hoverBox);
                        hoverBox.transform.position = new Vector3(node.x + 0.5f, node.CenterPoint.y + 0.02f, node.z + 0.5f);
                        hoverBoxes.Add(hBox);
                    }
                    hoverBox.SetActive(false);

                } 
                
            } else if (stateManager.highlightMode == HighlightMode.None) {

                // clear hover boxes
                foreach (GameObject hBox in hoverBoxes) {
                    Destroy(hBox);
                }
                hoverBoxes.Clear();
            }
        }

        // meteor shower
        if (MeteorShowerTimer <= 0 && StartMeteorShowerTimer) {
            //StartCoroutine(MeteorShowerRoutine());
        }

        */
    }

    //
    public void SetHighlightedTile(TileOld tile) {

        return;

        if (tile == null) { return; } 

        GameState mode = WorldStateManager.access.GameState;
        switch (mode) {

            case GameState.Default:

                hoverBox.transform.position = tile.origin.ToVector3() + new Vector3(0.5f, 0, 0.5f);

                break;

            case GameState.Build:

                Point origin = new Point(tile.origin.ToVector3());
                bool validMove = Wall.Blockable.ValidateMove(origin);
                Wall.Blockable.MoveBlockable(origin);

                if (validMove) {
                    Wall.SetMaterial(Resources.Load("Materials/ValidateGreen") as Material);
                } else {
                    Wall.SetMaterial(Resources.Load("Materials/ValidateRed") as Material);
                }

                break;

            case GameState.Place:

                origin = new Point(tile.origin.ToVector3());
                validMove = Cannon.Blockable.ValidateMove(origin);
                Cannon.Blockable.MoveBlockable(origin);

                if (validMove) {
                    Cannon.SetMaterial(Resources.Load("Materials/ValidateGreen") as Material);
                }
                else {
                    Cannon.SetMaterial(Resources.Load("Materials/ValidateRed") as Material);
                }

                break;

        }
    }

    //
    void ProcessStateChange(GameState state) {

        if (Wall != null) {
            SetHighlightedTile(WorldStateManager.access.highlightedTile);
        }

        if (Cannon != null) {
            Cannon.Move(GridData.offMapPoint);
        }

    }

    //
    void AddEffect(EffectType type, List<TileOld> tiles) {
        
        Effect effect = new Effect(tiles, type);
        Effects.Add(effect);

    }

    //
    void RemoveEffect(Effect effect) {
        Effects.Remove(effect);
        effect.Remove();
    }

    //
    public void RemoveEffectsByType(EffectType type) {

        List<Effect> removedEffects = Effects.FindAll((e) => { return e.Type == type; });
        foreach (var effect in removedEffects) {
            effect.Remove();
        }
    }

}
