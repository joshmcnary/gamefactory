﻿/*
    MapData generates and stores the MapNodes of the world
*/

using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Mirror;

public enum GeoRegionType { Circle, CornerSquare, CenterSquare }

[Serializable]
public class GridData : NetworkBehaviour {

    public event Action<TileOld[,]> MapDataGenerated;

    public static GridData access;

    public static Point  offMapPoint;
    public static Tile offMapNode;                // this is the node where entities will be if outside world
        
    [SyncVar]
    public int Size;
    [SyncVar]
    public int Width;
    [SyncVar]
    public int Height;
    float MinHeight;
    float MaxHeight;
    public float[,] HeightMap { get; set; }
    public float[,] NormalizedHeightMap { get; set; }
    public int HeightLevels { get; set; }
    public int MapDiagonalDistance;

    public TileOld[,] TilesOld;

    public TileList Tiles = new TileList();

    //
    public void Init() {

        offMapPoint = new Point(-100, -100);
        offMapNode = new Tile((int)offMapPoint.x, (int)offMapPoint.z);
        offMapNode.nullNode = true;

        // callback whenever tilelist receives message
        if (World.access.isClient) {
            Tiles.Callback += TileListUpdated;
        }
    }
    
    void TileListUpdated(SyncList<Tile>.Operation op, int index, Tile item) {

        UI.access.AddNetworkOutputText(op.ToString());
        switch (op) {
            case SyncList<Tile>.Operation.OP_ADD:       // add is never used on client as far as i can tell  
                OnMapDataGenerated();
                break;
            case SyncList<Tile>.Operation.OP_SET:
                OnMapDataGenerated();
                if (isClientOnly) {
                    EntityManager.access.SetBlockingObjectOnTile((int)item.coords.x, (int)item.coords.z, item.BlockingObject);
                }
                break;
        }
    }

    //
    public void InitSettings(WorldSettings settings) {

        Size = settings.MapSize;

        //HeightLevels = 2;

    }

    //
    public void Run() {

        // 128 is the max supported by textures, there seems to be a limit of vertices for mesh at 128x128 = 32768
        // 512 is max supported by networking with only tile data, 1024 failed to send messages anymore
        Size = 16;

        GenerateNewMap();

        //World.access.Camera.SetTargetPoint(new Vector3(Size / 2, 0, Size / 2));
    }

    //
    public void LoadData(WorldData data) {

        /*
        int size = data.Size;
        MapNode[,] nodes = data.Nodes;

        CreateGeoMap(size, size, nodes);
        World.access.Camera.SetTargetPoint(new Vector3(size / 2, 0, size / 2));

        GeoView.access.CreateViewMesh();
        */
    }

    //
    public void GenerateNewMap() {

        Width = Size;
        Height = Size;

        MapDiagonalDistance = (int)(Width * Mathf.Sqrt(2));

        GenerateNodes();

        OnMapDataGenerated();
    }

    // if nodes is null, new nodes are generated with heights/resources
    // also would be used when loading a game from a saved file 
    public void GenerateNodes(TileOld[,] nodes = null) {

        if (nodes != null) {
            TilesOld = nodes;
        } else {

            //
            for (int z = 0; z < Height; z++) {
                for (int x = 0; x < Width; x++) {

                    Tile tile = new Tile();
                    tile.coords = new Point(x, z);
                    Tiles.Add(tile);
                }
            }

            //InitHeightMap();
            //InitNodeResources();

        }

        GeneratePlayerIslands();

        // we dont want to add the callback on server until all the tiles are added because unlike client, they do not come in a single message
        // this means view will try to update on every tile added and error out because its trying to draw the whole texture with only 1 tile added
        if (!World.access.isClient) {
            Tiles.Callback += TileListUpdated;
        }

    }

    //
    void GeneratePlayerIslands() {

        int islandWidth = 8;
        int islandHeight = 8;

        Point player1Origin = new Point(Width / 4, Height / 4);
        Point player2Origin = new Point(3 * Width / 4, Height / 4);

        for (int z = 0; z < Height; z++) {
            for (int x = 0; x < Width; x++) {
                Tile node = Tiles[z * Width + x];

                bool nodeIsInPlayer1Width = x >= player1Origin.x - islandWidth / 2 && x <= player1Origin.x + islandWidth / 2;
                bool nodeIsInPlayer1Height = z >= player1Origin.z - islandHeight / 2 && z <= player1Origin.z + islandHeight / 2;

                bool nodeIsInPlayer2Width = x >= player2Origin.x - islandWidth / 2 && x <= player2Origin.x + islandWidth / 2;
                bool nodeIsInPlayer2Height = z >= player2Origin.z - islandHeight / 2 && z <= player2Origin.z + islandHeight / 2;

                if ((nodeIsInPlayer1Width && nodeIsInPlayer1Height) || (nodeIsInPlayer2Width && nodeIsInPlayer2Height)) {
                    node.type = TileType.Grass;
                } else {
                    node.type = TileType.Water;
                }

                Tiles[z * Width + x] = node;
            }
        }

        //
        for (int z = 0; z < GridData.access.Size; z++) {
            for (int x = 0; x < GridData.access.Size; x++) {
                int createDecision = UnityEngine.Random.Range(0, 2);
                if (createDecision == 0) {
                    GridData.access.GetTileNew(x, z).SetBlockingObject(BlockingObject.Tree);
                }
                else {
                    GridData.access.GetTileNew(x, z).SetBlockingObject(BlockingObject.Stone);
                }

            }
        }
    }

    // returns tile based off a point
    public Tile GetTileNew(int x, int z) {

        if (x == -100)
            return offMapNode;

        if (x < 0 || x >= Width || z < 0 || z >= Height) {
            Tile dummyNode = new Tile(x, z);
            dummyNode.nullNode = true;
            /*
            dummyNode.SetVerticesPositions(new MapPoint[5] {
                new MapPoint(x, z),
                new MapPoint(x + 1, z),
                new MapPoint(x, z + 1),
                new MapPoint(x + 1, z + 1),
                new MapPoint(x + 0.5f, z + 0.5f)                
            });
            */

            return dummyNode;
        }
        else {
            return Tiles.GetTile(x, z);
        }
    }

    // returns tile based off a point
    public Tile GetTileNew(Point point) {
        int x = Mathf.FloorToInt(point.x);
        int z = Mathf.FloorToInt(point.z);

        /*
        if (x == -100)
            return offMapNode;

        if (x < 0 || x >= Width || z < 0 || z >= Height) {
            Tile dummyNode = new Tile(x, z);
            dummyNode.nullNode = true;
            /*
            dummyNode.SetVerticesPositions(new MapPoint[5] {
                new MapPoint(x, z),
                new MapPoint(x + 1, z),
                new MapPoint(x, z + 1),
                new MapPoint(x + 1, z + 1),
                new MapPoint(x + 0.5f, z + 0.5f)                
            });
            */

        /*
            return dummyNode;
        }
        else {
            return Tiles[x, z];
        }
        */

        return Tiles[z * Height + x];
    }

    //
    public void SetTile(int x, int z, Tile tile) {

        Tiles[z * Width + x] = tile;
    }  

    // returns all neighbors of the node in parameter
    // reurns bottom, left, top, right
    public TileOld[] GetNeighborNodes(TileOld node) {

        int x = node.x;
        int z = node.z;

        TileOld[] neighborNodes = new TileOld[4];

        if (z > 0) { neighborNodes[0] = TilesOld[x, z - 1]; }
        if (x > 0) { neighborNodes[1] = TilesOld[x - 1, z]; }
        if (z < Height - 1) { neighborNodes[2] = TilesOld[x, z + 1]; }
        if (x < Width - 1) { neighborNodes[3] = TilesOld[x + 1, z]; }

        return neighborNodes;

    }

    // returns all tiles surrounding node in parameter
    // reurns bottom, botleft, left, topleft, top, topright, right, botright
    public List<TileOld> GetSurroundingTiles(TileOld tile, Func<TileOld, bool> checkCondition = null) {

        int x = tile.x;
        int z = tile.z;

        List<TileOld> surroundingTiles = new List<TileOld>();

        if (z > 0) { surroundingTiles.Add(TilesOld[x, z - 1]); }                                  // bot
        if (z > 0 && x > 0) { surroundingTiles.Add(TilesOld[x - 1, z - 1]); }                     // botleft
        if (x > 0) { surroundingTiles.Add(TilesOld[x - 1, z]); }                                  // left
        if (x > 0 && z < Height - 1) { surroundingTiles.Add(TilesOld[x - 1, z + 1]); }            // topleft
        if (z < Height - 1) { surroundingTiles.Add(TilesOld[x, z + 1]); }                         // top
        if (x < Width - 1 && z < Height - 1) { surroundingTiles.Add(TilesOld[x + 1, z + 1]); }    // topright
        if (x < Width - 1) { surroundingTiles.Add(TilesOld[x + 1, z]); }                          // right
        if (x < Width - 1 && z > 0) { surroundingTiles.Add(TilesOld[x + 1, z - 1]); }             // botright

        if (checkCondition != null) {
            for (int i = 0; i < surroundingTiles.Count; i++) {
                TileOld checkTile = surroundingTiles[i];
                if (checkCondition (checkTile)) {
                    continue;
                } else {
                    surroundingTiles.RemoveAt(i);
                    i--;
                }

            }
        }

        return surroundingTiles;

    }


    // returns an array of geonodes based off of given parameters
    // @param selectionMode -- square or circle
    /*
    public GeoRegion GetGeoRegion(GeoRegionType regionType, List<MapNode> originNodes, int size) {

        List<MapNode> regionNodes = new List<MapNode>();

        switch (regionType) {


            case GeoRegionType.CornerSquare:

                // origin ( nodes[0] ) is left / bottom point of list
                MapNode originNode = originNodes[0];

                for (int z = 0; z <= size; z++) {
                    for (int x = 0; x <= size; x++) {
                        int nodeX = originNode.x + x;
                        int nodeZ = originNode.z + z;

                        if (nodeX < 0 || nodeX >= width) { continue; }
                        if (nodeZ < 0 || nodeZ >= height) { continue; }
                        regionNodes.Add(Nodes[nodeX, nodeZ]);
                    }
                }

                break;

            case GeoRegionType.CenterSquare:

                // add a way of selecting a square with an origin, this means the length/width will always be odd, so even sized squares should use cornersquare
                for (int x = -size; x <= size; x++) {
                    for (int z = -size; z <= size; z++) {
                        int nodeX = originNodes[0].x + x;
                        int nodeZ = originNodes[0].z + z;

                        if (nodeX < 0 || nodeX >= width) { continue; }
                        if (nodeZ < 0 || nodeZ >= height) { continue; }
                        regionNodes.Add(Nodes[nodeX, nodeZ]);
                    }
                }

                break;

            case GeoRegionType.Circle:

                // origin is center point

                for (int x = -size; x <= size; x++) {
                    for (int z = -size; z <= size; z++) {
                        int nodeX = originNodes[0].x + x;
                        int nodeZ = originNodes[0].z + z;

                        if (nodeX < 0 || nodeX >= width) { continue; }
                        if (nodeZ < 0 || nodeZ >= height) { continue; }

                        bool circleTest = Mathf.Pow(nodeX - originNodes[0].x, 2) + Mathf.Pow(nodeZ - originNodes[0].z, 2) <= Mathf.Pow(size, 2);
                        if (circleTest) {
                            regionNodes.Add(Nodes[nodeX, nodeZ]);
                        }
                    }
                }

                

                // find top nodes
                List<Index> topZNodes = new List<Index>();
                for (int i = 0; i < regionNodes.Count; i++) {

                    int tempX = regionNodes[i].x;
                    int tempZ = regionNodes[i].z;

                    bool xExists = false;
                    for (int j = 0; j < topZNodes.Count; j++) {

                        if (topZNodes[j].x == tempX) {
                            xExists = true;
                            if (topZNodes[j].z < tempZ) {
                                topZNodes[j].z = tempZ;
                            }
                        }
                    }

                    if (!xExists) {
                        topZNodes.Add(new Index(tempX, tempZ));
                    }

                }
                
                // determine how far to extend circle upward
                int topZ = -1;
                for (int i = 0; i < originNodes.Count; i++) {

                    if (originNodes[i].z > topZ)
                        topZ = originNodes[i].z;
                }

                // extend the circle upward
                int zDiff = topZ - topZNodes[0].z;
                for (int i = 0; i < topZNodes.Count; i++) {
                    Index index = topZNodes[i];
                    for (int z = 1; z <= zDiff; z++) {
                        regionNodes.Add(Nodes[index.x, index.z + z]);
                    }
                }


                // find right nodes
                List<Index> rightXNodes = new List<Index>();
                for (int i = 0; i < regionNodes.Count; i++) {

                    int tempX = regionNodes[i].x;
                    int tempZ = regionNodes[i].z;

                    bool zExists = false;
                    for (int j = 0; j < rightXNodes.Count; j++) {

                        if (rightXNodes[j].z == tempZ) {
                            zExists = true;
                            if (rightXNodes[j].x < tempX) {
                                rightXNodes[j].x = tempX;
                            }
                        }
                    }

                    if (!zExists) {
                        rightXNodes.Add(new Index(tempX, tempZ));
                    }

                }

                // determine how far to extend circle to the right
                int rightX = -1;
                for (int i = 0; i < originNodes.Count; i++) {

                    if (originNodes[i].x > rightX)
                        rightX = originNodes[i].x;
                }

                // extend the circle to the right
                int xDiff = rightX - originNodes[0].x;
                for (int i = 0; i < rightXNodes.Count; i++) {
                    Index index = rightXNodes[i];
                    for (int x = 1; x <= xDiff; x++) {
                        regionNodes.Add(Nodes[index.x + x, index.z]);
                    }
                }


                break;
        }

        //
        GeoRegion region = new GeoRegion(regionType, originNodes, size, regionNodes);
        return region;
    }

    // shortcut for single node origin
    public GeoRegion GetGeoRegion(GeoRegionType regionType, MapNode origin, int size) {
        return GetGeoRegion(regionType, new List<MapNode>() { origin }, size);
    }
    */

    //
    public void InitializeFloodFillAlgorithm(TileOld initialTile, bool useAdjacentTiles = true) {

        var adjacentToInitial = new List<TileOld>();
        if (useAdjacentTiles) {
            int numOfWallsNear = 0;
            adjacentToInitial = GetSurroundingTiles(initialTile, (checkTile) => {
                if (checkTile.blockableEntity != null) {
                    numOfWallsNear++;
                }
                return !checkTile.nullNode && checkTile.blockableEntity == null;
            });

            if (numOfWallsNear < 2 && adjacentToInitial.Count > 0) {
                adjacentToInitial = new List<TileOld>() { adjacentToInitial[0] };
            }

        }
        else {
            adjacentToInitial.Add(initialTile);
        }

        //run the algorithm for all 4 tiles around the initial tile.
        foreach (var t in adjacentToInitial) {
            var ffa = new FloodFillAlgorithm();
            ffa.StartFillAlgorithm(t,
                onFinish: (alg) => {
                    if (!alg.outOfBounds) {
                        List<TileOld> roomTiles = new List<TileOld>(alg.openList);
                        Room room = new Room(roomTiles);
                        WorldStateManager.access.AddRoom(room);
                    }
                    else {
                        foreach (var a in alg.openList) {
                            //a.SetPlayerColor(Color.white);
                        }
                    }
                }
            );

            //if (!ffa.outOfBounds)
            //{
            //    foreach (var o in ffa.openList)
            //    {
            //        o.SetPlayerColor(Color.cyan);
            //    }
            //}
        }
    }

    //
    protected void OnMapDataGenerated() {
        MapDataGenerated?.Invoke(TilesOld);
    }
}

public class FloodFillAlgorithm {

    public bool outOfBounds = false;
    public HashSet<TileOld> searchHash = new HashSet<TileOld>();
    public HashSet<TileOld> openList = new HashSet<TileOld>();
    public HashSet<TileOld> blockedList = new HashSet<TileOld>();

    public Action<FloodFillAlgorithm> finishedCallback = null;

    int searchCount = 0;
    int searchLimit = 500;

    public void StartFillAlgorithm(TileOld initialTile, Action<FloodFillAlgorithm> onFinish) {
        finishedCallback = onFinish;
        searchHash.Add(initialTile);
        SearchForMore();
    }

    public void SearchForMore() {
        searchCount++;
        if (searchCount > searchLimit) {
            Debug.LogError("over search limit");
            FinishedAlgorithm();
            return;
        }
        if (searchHash.Count > 0) {
            var tile = searchHash.First();
            if (tile.nullNode) {
                outOfBounds = true;
            }
            if (tile.blockableEntity != null) {
                if (!blockedList.Contains(tile)) {
                    blockedList.Add(tile);
                }
            }
            if (tile.blockableEntity == null) {

                if (!openList.Contains(tile)) {
                    openList.Add(tile);
                }
                var adjacentTiles = GridData.access.GetSurroundingTiles(tile);
                for (int m = 0; m < adjacentTiles.Count; m++) {
                    if (adjacentTiles[m].nullNode || adjacentTiles[m].type == TileType.Water) {
                        outOfBounds = true;
                        continue;
                    }
                    if (!searchHash.Contains(adjacentTiles[m]) &&
                        !blockedList.Contains(adjacentTiles[m]) &&
                        !openList.Contains(adjacentTiles[m])) {
                        searchHash.Add(adjacentTiles[m]);
                    }
                }
            }

            searchHash.Remove(tile);
        }
        else {
            //done
            //Debug.Break();
            FinishedAlgorithm();
            return;
        }

        //if (!outOfBounds)
        //{
        SearchForMore();
        //}
    }

    public void FinishedAlgorithm() {
        if (finishedCallback != null) {
            finishedCallback(this);
        }
    }
}


public class TileList : SyncList<Tile> {
    
    public void Set(int x, int y, Tile tile) {
        //this.ElementAt(y * MapData.)
    }

    public Tile GetTile(int x, int z) {
        return this.ElementAt(z * GridData.access.Width + x);
    }
}

public class PointList : SyncList<Point> { }
