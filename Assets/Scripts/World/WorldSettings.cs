﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

// world settings are used for initial settings for managers
// this can be used to set up unit testing scenarios, and save specific setups to test specific functionality such as movables testing or specific entity testing
public class WorldSettings : Settings {

    //[Header("Resource Settings")]
    //public ResourceAmount StartingResources;
    
    [Header("Map Settings")]
    public int MapSize = 8;
    public float NodeSize = 1;
    //public bool UseHeight = false;

    /*
    [Header("Test Settings")]
    public bool FastBuild = true;
    public bool DisableWinLoss = true;
    public int GaeanGenerationRate = 0;
    public int MaxGaeans = 0;
    public int GaeanWinCondition = 50;
    public bool DisastersEnabled = false;
    public int DisasterInterval = 120;
    */

    public virtual void LastRunScript() {

        WorldStateManager.access.SetGameState(GameState.Place);
        //EntityManager.access.AddNewEntityToGame(EntityType.Castle, new Point(8, 8), 0);

        //MapData.access.InitializeFloodFillAlgorithm(MapData.access.GetTile(8, 8));

    }

}

//
[Serializable]
public class EntityDataModel {
    /*
    public EntityType entityType;
    public GeoPoint entityOrigin;
    */

}

//public enum ResourceAmount { Massive, Standard, Tutorial }
