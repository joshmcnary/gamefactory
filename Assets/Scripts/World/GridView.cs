﻿using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.AI;
using Mirror;

public class GridView : Manager, IPointerDownHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

    MeshRenderer MeshRenderer;
    //MapGenerator MapGenerator;

    public static GridView access;

    public event System.Action<Tile> TileLeftPressed;
    public event System.Action<Tile> NodeRightPressed;
    public event System.Action<Vector3> LeftClick;
    public event System.Action<Vector3> RightClick;

    bool MapGenerated = false;          // used to prevent create view mesh from constantly running

    public MeshFilter MeshFilter;
    //public NavMeshSurface HumanNavMesh;
    //public NavMeshSurface MachineNavMesh;

    public MapNodeView[,] ViewNodes { get; set; }

    public float heightFactor = 0.7f;
    public int scale = 1;
    int VertsPerSide = 2;       // this number should be odd, so there is a center point and an even amount of inner squares
    int VertsPerNode;      

    [Header("Grid Lines")]
    public bool gridLinesFlag = true;
    public GameObject gridLines;
    public float lineWidth = 0.01f;

    [Header("Labels")]
    public bool EnableLabels;
    public GeoViewLabel GeoViewLabelType;
    public enum GeoViewLabel { Position, Height }

    [Header("Path Mesh")]
    public GameObject PathMeshObject;

    void Start() {
        FindObjectOfType<UI>().AddNetworkOutputText("gridview started");
    }    

    //
    public void Init() {

        //MapGenerator = GeoMap.access.MapGenerator;
        MeshRenderer = GetComponent<MeshRenderer>();
        MeshFilter = GetComponent<MeshFilter>();

        // for determining agent type ids
        for (int i = 0; i < NavMesh.GetSettingsCount(); i++) {
            NavMeshBuildSettings settings = NavMesh.GetSettingsByIndex(i);
        }

        VertsPerNode = (int)Mathf.Pow(VertsPerSide, 2) + (int)Mathf.Pow(VertsPerSide - 1, 2);

        GridData.access.MapDataGenerated += delegate { CreateViewMesh(); };

        //World.access.MapData.TilesTest.Callback += delegate(SyncList<TileNew>.Operation o, int index, TileNew tile) { Debug.Log(tile); };

    }

    // 
    public override void Run() {

        CreateViewMesh();

    }

    //
    public override void LoadSettings(WorldSettings settings) {
        int mapSize = World.access.Settings.MapSize;
        ViewNodes = new MapNodeView[mapSize, mapSize];
    }

    // method for IPointerDownHandler
    public void OnPointerDown(PointerEventData pointerData) {

        // convert world position to node
        Vector3 worldPosition = pointerData.pointerCurrentRaycast.worldPosition;
        Tile pressedNode = ViewNodes[(int)worldPosition.x, (int)worldPosition.z].Tile;

        if (Input.GetMouseButtonDown(0)) {
            OnNodeLeftPressed(pressedNode);
        } else if (Input.GetMouseButtonDown(1)) {
            OnNodeRightPressed(pressedNode);
        }
    }

    //
    public void OnNodeLeftPressed(Tile pressedNode) {
        TileLeftPressed?.Invoke(pressedNode);
    }

    //
    public void OnNodeRightPressed(Tile pressedNode) {
        NodeRightPressed?.Invoke(pressedNode);
    }

    // method for IPointerDownHandler
    public void OnPointerClick(PointerEventData pointerData) {
        /*
        GeoViewEventArgs args = new GeoViewEventArgs();
        args.worldPosition = pointerData.pointerCurrentRaycast.worldPosition;
        if (Input.GetMouseButtonUp(0)) {
            OnLeftClick(args);
        } else if (Input.GetMouseButtonUp(1)) {
            OnRightClick(args);
        }
        */
    }

    // left click event
    /*
    protected virtual void OnLeftClick(GeoViewEventArgs e) {
        if (LeftClick != null)
            LeftClick(this, e);
    }

    // right click event
    protected virtual void OnRightClick(GeoViewEventArgs e) {
        if (RightClick != null)
            RightClick(this, e);
    }
    */

    //
    public void OnPointerEnter(PointerEventData pointerData) {
        World.access.State.SetHighlightMode(HighlightMode.Map);
    }

    public void OnPointerExit(PointerEventData pointerData) {
        if (pointerData.pointerCurrentRaycast.gameObject == null) {
            World.access.State.SetHighlightMode(HighlightMode.None);
        }
        World.access.State.highlightedTile = null;
    }
    

    // creates a flat surface with MapWidth * MapHeight nodes
    // each node has a number of verts on each side set by VertsPerSide property
    // this method should be called by geomap so that everytime the data changes, a new mesh is created
    public void CreateViewMesh() {

        if (MapGenerated) { return; }

        GridData map = World.access.GridData;

        int MapWidth = map.Width;
        int MapHeight = map.Height;
        ViewNodes = new MapNodeView[MapWidth, MapHeight];
        //Tile[,] nodes = map.Tiles;
        TileList nodes = map.Tiles;
        //TilePrefab[,] nodes = map.TilePrefabTest;

        // setup arrays for verts, uv, and tris
        Mesh mesh = new Mesh();    
        int totalVertCount = VertsPerNode + (VertsPerNode - VertsPerSide) * (MapWidth - 1) * 2 + (VertsPerNode - (2 * VertsPerSide - 1)) * (int)Mathf.Pow(MapWidth - 1, 2);
        Vector3[] vertices = new Vector3[totalVertCount];
        Vector2[] uv = new Vector2[totalVertCount];
        int[] tris = new int[(int)Mathf.Pow(VertsPerSide - 1, 2) * 12 * MapWidth * MapHeight];


        // create vertices
        int vertCounter = 0;
        for (int z = 0; z < MapHeight; z++) {
            for (int x = 0; x < MapWidth; x++) {

                //Tile currentNode = nodes[x, z];
                //TileNew currentNode = nodes[z * MapWidth + x];
                Tile currentNode = nodes[z * MapWidth + x];
                MapNodeView viewNode = new MapNodeView(currentNode);

                // add vertices needed for new node
                Vector3[] verts = GetNextCreatedVertices(new Point(x, z));
                for (int i = 0; i < verts.Length; i++) {
                    vertices[vertCounter] = verts[i];
                    vertCounter++;
                }

                // setup vertex indices for node
                int[] vertIndices = new int[VertsPerNode];
                int[] rightSideVertIndices = new int[VertsPerSide];
                int[] topSideVertIndices = new int[VertsPerSide];

                if (x == 0 && z == 0) { // origin node

                    // all indices
                    for (int i = 0; i < VertsPerNode; i++) {
                        vertIndices[i] = i;
                    }

                }
                else if (z == 0) {      // bottom row nodes
                    
                    // get right side vertices from node to the left to use as the starting point for vector indices
                    int[] rightSideIndices = ViewNodes[x - 1, z].RightSideVertIndices;
                    int lastVertIndex = rightSideIndices[VertsPerSide - 1];
                    int currentVertIndex = lastVertIndex + 1;

                    for (int i = 0; i < rightSideIndices.Length; i++) {
                        
                        vertIndices[i * (2 * VertsPerSide - 1)] = rightSideIndices[i];

                        for (int j = 1; j < (2 * VertsPerSide - 1); j++) {

                            // the last pass is too long because of center points
                            if (i * (2 * VertsPerSide - 1) + j >= vertIndices.Length) { break; }

                            vertIndices[i * (2 * VertsPerSide - 1) + j] = currentVertIndex;
                            currentVertIndex++;
                        }
                    }

                } else if (x == 0) {    // left column nodes

                    // get top side vertices from node to the bottom to use as the starting point for vector indices
                    int[] topSideIndices = ViewNodes[x, z - 1].TopSideVertIndices;
                    int lastVertIndex = ViewNodes[MapWidth - 1, z - 1].VertIndices[VertsPerNode - 1];
                    int currentVertIndex = lastVertIndex + 1;

                    // bottom vertices row of the node
                    for (int i = 0; i < topSideIndices.Length; i++) {
                        vertIndices[i] = topSideIndices[i];
                    }

                    // first center points row
                    for (int i = topSideIndices.Length; i < topSideIndices.Length + VertsPerSide - 1; i++) {
                        vertIndices[i] = currentVertIndex;
                        currentVertIndex++;
                    }

                    // remaining vertices
                    for (int i = 1; i < VertsPerSide; i++) {

                        for (int j = 0; j < 2 * VertsPerSide - 1; j++) {

                            // the last pass is too long because of middle points
                            if (i * (2 * VertsPerSide - 1) + j >= vertIndices.Length) { break; }

                            vertIndices[i * (2 * VertsPerSide - 1) + j] = currentVertIndex;
                            currentVertIndex++;
                        }
                    }

                } else {

                    // get top side vertices from node to the bottom to use as the starting point for vector indices
                    int[] rightSideIndices = ViewNodes[x - 1, z].RightSideVertIndices;
                    int[] topSideIndices = ViewNodes[x, z - 1].TopSideVertIndices;
                    int lastVertIndex = rightSideIndices[VertsPerSide - 1];
                    int currentVertIndex = lastVertIndex + 1;

                    // bottom vertices row of the node
                    for (int i = 0; i < topSideIndices.Length; i++) {
                        vertIndices[i] = topSideIndices[i];
                    }

                    // first center points row
                    for (int i = topSideIndices.Length; i < topSideIndices.Length + VertsPerSide - 1; i++) {
                        vertIndices[i] = currentVertIndex;
                        currentVertIndex++;
                    }

                    // remaining vertices
                    for (int i = 1; i < VertsPerSide; i++) {

                        vertIndices[i * (2 * VertsPerSide - 1)] = rightSideIndices[i];

                        for (int j = 1; j < 2 * VertsPerSide - 1; j++) {

                            // the last pass is too long because of middle points
                            if (i * (2 * VertsPerSide - 1) + j >= vertIndices.Length) { break; }

                            vertIndices[i * (2 * VertsPerSide - 1) + j] = currentVertIndex;
                            currentVertIndex++;
                        }
                    }

                }

                // for storing right side indices in current node
                for (int i = 0; i < VertsPerSide; i++) {
                    rightSideVertIndices[i] = vertIndices[VertsPerSide + ((2 * VertsPerSide - 1) * i) - 1];
                }

                // for storing top side indices in current node
                for (int i = 0; i < VertsPerSide; i++) {
                    topSideVertIndices[i] = vertIndices[VertsPerNode - VertsPerSide + i];
                }

                viewNode.SetVertIndices(vertIndices);
                viewNode.RightSideVertIndices = rightSideVertIndices;
                viewNode.TopSideVertIndices = topSideVertIndices;

                ViewNodes[x, z] = viewNode;

            }
        }
        mesh.vertices = vertices;

        // create uv
        int uvIndex = 0;
        for (int i = 0; i < uv.Length; i++) {
            Vector3 vertPos = vertices[i];
            Vector2 tempUv = new Vector2(vertPos.x / MapWidth, vertPos.z / MapHeight);
            uv[uvIndex] = tempUv;
            uvIndex++;
        }
        mesh.uv = uv;


        // create tris
        // every node will have (VertsPerSide - 1)^2 inner squares and each inner square will have 4 triangles
        int triIndex = 0;
        for (int z = 0; z < MapHeight; z++) {
            for (int x = 0; x < MapWidth; x++) {


                int[] vectorIndices = ViewNodes[x, z].VertIndices; 

                for (int nodeZ = 0; nodeZ < VertsPerSide - 1; nodeZ++) {
                    for (int nodeX = 0; nodeX < VertsPerSide - 1; nodeX++) {

                        //Debug.Log(nodeX + ", " + nodeZ);

                        // bottom tri
                        tris[triIndex] = vectorIndices[nodeX + 1 + nodeZ * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + nodeZ * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + nodeZ * (2 * VertsPerSide - 1) + VertsPerSide];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;


                        // left tri
                        tris[triIndex] = vectorIndices[nodeX + nodeZ * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + (nodeZ + 1) * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + nodeZ * (2 * VertsPerSide - 1) + VertsPerSide];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;

                        // top tri
                        tris[triIndex] = vectorIndices[nodeX + (nodeZ + 1) * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + 1 + (nodeZ + 1) * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + nodeZ * (2 * VertsPerSide - 1) + VertsPerSide];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;

                        // left tri
                        tris[triIndex] = vectorIndices[nodeX + 1 + (nodeZ + 1) * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + 1 + nodeZ * (2 * VertsPerSide - 1)];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;
                        tris[triIndex] = vectorIndices[nodeX + nodeZ * (2 * VertsPerSide - 1) + VertsPerSide];
                        //Debug.Log(tris[triIndex]);
                        triIndex++;

                    }
                }
            }
        }
        mesh.triangles = tris;

        // this makes it so shadows work
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        // set surface mesh to show created mesh
        MeshFilter.mesh = mesh;

        //
        UpdateMeshCollider();

        // set heights -- this should be done in mapdata
        //SetSurfaceHeights();

        //
        SetViewTexture();

        // add grid lines
        if (gridLinesFlag) {
            DrawGridLines();
        }

        //
        //BuildNavMesh();

        //
        //ShowLabels();
        UI.access.AddNetworkOutputText("test");

        MapGenerated = true;
    }

    //
    /*
    public void BuildNavMesh() {
        if (HumanNavMesh.navMeshData == null) {
            HumanNavMesh.BuildNavMesh();
            MachineNavMesh.BuildNavMesh();
        } else {

            List<NavMeshBuildSource> sources = HumanNavMesh.CollectSources();
            var sourceBounds = new Bounds(new Vector3(4, 0, 4), NavMeshSurface.Abs(HumanNavMesh.size * 2));
            NavMeshBuilder.UpdateNavMeshDataAsync(HumanNavMesh.navMeshData, HumanNavMesh.GetBuildSettings(), sources, sourceBounds);
            
            List<NavMeshBuildSource> _sources = MachineNavMesh.CollectSources();
            var _sourceBounds = new Bounds(new Vector3(4, 0, 4), NavMeshSurface.Abs(MachineNavMesh.size * 4));
            NavMeshBuilder.UpdateNavMeshDataAsync(MachineNavMesh.navMeshData, MachineNavMesh.GetBuildSettings(), _sources, _sourceBounds);

        }
    }
    */

    // gets vector3 positions of vertices that need to be created next based on bot left vertex
    // only used for mesh creation 
    private Vector3[] GetNextCreatedVertices(Point point) {

        float x = point.x;
        float z = point.z;        

        // determine number of verts needed
        int vertCount = 0;
        if (x == 0 && z == 0) {
            vertCount = VertsPerNode;
        }
        else if (z == 0 || x == 0) {
            vertCount = VertsPerNode - VertsPerSide;
        }
        else {
            vertCount = VertsPerNode - 2 * VertsPerSide + 1;
        }        

        Vector3[] vertices = new Vector3[vertCount];
        int index = 0;

        // bottom left node
        if (x == 0 && z == 0) {

            for (int _z = 0; _z < VertsPerSide; _z++) {

                // bottom row of vertices
                for (int _x = 0; _x < VertsPerSide; _x++) {
                    vertices[index] = new Vector3(_x * (1f / (VertsPerSide - 1)), 0, _z * (1f / (VertsPerSide - 1)));
                    index++;
                }

                // middle point -- ignore if doing the top row of vectors
                if (_z < VertsPerSide - 1) {
                    for (int _x = 0; _x < VertsPerSide - 1; _x++) {
                        vertices[index] = new Vector3(
                            (_x * 1f / (VertsPerSide - 1) + (_x + 1 * 1f) / (VertsPerSide - 1)) / 2,
                            0,
                            (_z * 1f / (VertsPerSide - 1) + (_z + 1 * 1f) / (VertsPerSide - 1)) / 2);
                        index++;
                    }
                }
            }

        // bottom row nodes
        } else if (z == 0) {

            for (int _z = 0; _z < VertsPerSide; _z++) {

                // bottom row of vertices
                for (int _x = 1; _x < VertsPerSide; _x++) {
                    vertices[index] = new Vector3(x + _x * (1f / (VertsPerSide - 1)), 0, z + _z * (1f / (VertsPerSide - 1)));
                    index++;
                }

                // middle point -- ignore if doing the top row of vectors
                if (_z < VertsPerSide - 1) {
                    for (int _x = 0; _x < VertsPerSide - 1; _x++) {
                        vertices[index] = new Vector3(
                            x + (_x * 1f / (VertsPerSide - 1) + (_x + 1 * 1f) / (VertsPerSide - 1)) / 2,
                            0,
                            z + (_z * 1f / (VertsPerSide - 1) + (_z + 1 * 1f) / (VertsPerSide - 1)) / 2);
                        index++;

                    }
                }
            }

        }

        // left column nodes
        else if (x == 0) {

            for (int _z = 0; _z < VertsPerSide; _z++) {

                // bottom row of vertices, skip the first row since these will be top vertices of node down one
                if (_z != 0) {
                    for (int _x = 0; _x < VertsPerSide; _x++) {
                        vertices[index] = new Vector3(x + _x * (1f / (VertsPerSide - 1)), 0, z + _z * (1f / (VertsPerSide - 1)));
                        index++;
                    }
                } 

                // middle point -- ignore if doing the top row of vectors
                if (_z < VertsPerSide - 1) {
                    for (int _x = 0; _x < VertsPerSide - 1; _x++) {
                        vertices[index] = new Vector3(
                            x + (_x * 1f / (VertsPerSide - 1) + (_x + 1 * 1f) / (VertsPerSide - 1)) / 2,
                            0,
                            z + (_z * 1f / (VertsPerSide - 1) + (_z + 1 * 1f) / (VertsPerSide - 1)) / 2);
                        
                        index++;

                    }
                }
            }
        }

        // all other nodes
        else {

            for (int _z = 0; _z < VertsPerSide; _z++) {

                // bottom row of vertices, skip the first row since these will be top vertices of node down one
                if (_z != 0) {
                    for (int _x = 1; _x < VertsPerSide; _x++) {
                        vertices[index] = new Vector3(x + _x * (1f / (VertsPerSide - 1)), 0, z + _z * (1f / (VertsPerSide - 1)));
                        index++;
                    }
                }

                // middle point -- ignore if doing the top row of vectors
                if (_z < VertsPerSide - 1) {
                    for (int _x = 0; _x < VertsPerSide - 1; _x++) {
                        vertices[index] = new Vector3(
                            x + (_x * 1f / (VertsPerSide - 1) + (_x + 1 * 1f) / (VertsPerSide - 1)) / 2,
                            0,
                            z + (_z * 1f / (VertsPerSide - 1) + (_z + 1 * 1f) / (VertsPerSide - 1)) / 2);
                        
                        index++;

                    }
                }
            }

        }

        return vertices;

    }

    //
    public void SetViewTexture() {

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        //Tile[,] nodes = MapData.access.Tiles;
        TileList nodes = GridData.access.Tiles;
        //TilePrefab[,] nodes = MapData.access.TilePrefabTest;

        int MapWidth = GridData.access.Width;
        int MapHeight = GridData.access.Height;

        Texture2D texture = new Texture2D(MapWidth, MapHeight);
        texture.wrapMode = TextureWrapMode.Clamp;

        Color[] colors = new Color[MapWidth * MapHeight];
        for (int z = 0; z < MapHeight; z++) {
            for (int x = 0; x < MapWidth; x++) {
                TileType type = nodes[z * MapWidth + x].type;
                colors[z * MapWidth + x] = type == TileType.Grass ? Color.green : Color.blue;
            }
        }

        texture.SetPixels(colors);
        texture.Apply();       

        MeshRenderer.material.mainTexture = texture;

        // Encode texture into PNG for debugging if needed
        //byte[] bytes = texture.EncodeToPNG();
        //File.WriteAllBytes(Application.dataPath + "/../MapViewTexture.png", bytes);

    }


    //
    public void SetSurfaceHeights() {        

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        TileOld[,] nodes = GridData.access.TilesOld;

        int MapWidth = GridData.access.Width;
        int MapHeight = GridData.access.Height;

        Texture2D texture = new Texture2D(MapWidth, MapHeight);
        texture.wrapMode = TextureWrapMode.Clamp;

        Color[] colors = new Color[MapWidth * MapHeight];

        Vector3[] newVerts = mesh.vertices;

        // clear all height data
        /*
        for (int i = 0; i < newVerts.Length; i++) {
            newVerts[i].y = 0;
        }

        // set all nodes to the floor of their height
        for (int z = 0; z < MapHeight; z++) {
            for (int x = 0; x < MapWidth; x++) {
                GeoNode currentNode = nodes[x, z];
                float nodeHeight = currentNode.Height;
                float height = nodeHeight * heightFactor;
                int[] indices = currentNode.VertIndices;

                for (int i = 0; i < indices.Length; i++) {
                    newVerts[indices[i]].y = Mathf.Floor(height);
                }
            }
        }

        // set all heights that are integers
        for (int z = 0; z < MapHeight; z++) {
            for (int x = 0; x < MapWidth; x++) {
                GeoNode currentNode = nodes[x, z];
                float nodeHeight = currentNode.Height;
                float height = nodeHeight * heightFactor;
                int[] indices = currentNode.VertIndices;
                
                if (nodeHeight % 1f == 0f) {
                    for (int i = 0; i < indices.Length; i++) {
                        newVerts[indices[i]].y = height;
                    }
                }
            }
        }
        

        // set up ramps / smooth out tiles
        GeoNode tempTile;
        float tempHeight;

        for (int z = 0; z < MapHeight; z++) {
            for (int x = 0; x < MapWidth; x++) {

                GeoNode currentNode = nodes[x, z];
                float nodeHeight = currentNode.Height;
                float scaledHeight = nodeHeight * heightFactor;
                int[] vertIndices = currentNode.VertIndices;
                int centerVert = vertIndices[currentNode.CenterVertIndex];
                GeoNode[] neighborNodes = GeoMap.access.GetNeighborNodes(currentNode);

                // determine number of levels neighboring current node as this helps make a determination on how the node vertices should be manipulated
                List<float> neighborLevels = new List<float>();
                Dictionary<float, int> neighborLevelCounts = new Dictionary<float, int>();
                for (int i = 0; i < neighborNodes.Length; i++) {
                    GeoNode neighborNode = neighborNodes[i];
                    if (neighborNode == null || neighborNode.Height % 1 != 0) { continue; }
                    if (!neighborLevelCounts.ContainsKey(neighborNode.Height)) {
                        neighborLevelCounts.Add(neighborNode.Height, 1);
                    } else {
                        neighborLevelCounts[neighborNode.Height]++;
                    }
                }

                // setup ramps
                if (nodeHeight % 1 == 0.5f && neighborLevelCounts.Keys.Count == 2) {

                    List<float> heightsList = new List<float>(neighborLevelCounts.Keys);

                    float lowerLevel = heightsList[0] < heightsList[1] ? heightsList[0] : heightsList[1];
                    float higherLevel = heightsList[0] > heightsList[1] ? heightsList[0] : heightsList[1];

                    float startPoint = 0, endPoint = 0;

                    // setup straight ramps -- 1 side adjacent to higher level
                    if (neighborLevelCounts[higherLevel] == 1) {

                        //Debug.Log(currentNode);

                        for (int i = 0; i < neighborNodes.Length; i++) {
                            GeoNode neighborNode = neighborNodes[i];
                            if (neighborNode == null) { continue; }

                            if (neighborNode.Height == higherLevel) {

                                int rowSize = 2 * VertsPerSide - 1;

                                // depending on which neighbor is the higher level, can make assumption about how each vertex can be positioned
                                switch (i) {
                                    case 0:

                                        startPoint = newVerts[vertIndices[currentNode.BotLeftVertIndex]].y;
                                        endPoint = newVerts[vertIndices[currentNode.BotRightVertIndex]].y;

                                        // go through each inner square and position the corners
                                        for (int j = 1; j < VertsPerSide - 1; j++) {
                                            for (int k = 0; k < VertsPerSide; k++) {
                                                int currentIndex = j * rowSize + k;
                                                newVerts[vertIndices[currentIndex]].y = Mathf.Lerp(startPoint, endPoint, 1 - newVerts[vertIndices[currentIndex]].z % 1f);
                                            }
                                        }
                                        break;
                                    case 1:                                        
                                        
                                        startPoint = newVerts[vertIndices[currentNode.BotLeftVertIndex]].y;
                                        endPoint = newVerts[vertIndices[currentNode.BotRightVertIndex]].y;

                                        // go through each inner square and position the corners
                                        for (int j = 1; j < VertsPerSide - 1; j++) {
                                            for (int k = 0; k < VertsPerSide; k++) {
                                                int currentIndex = j * rowSize + k;
                                                newVerts[vertIndices[currentIndex]].y = Mathf.Lerp(startPoint, endPoint, 1 - newVerts[vertIndices[currentIndex]].z % 1f);
                                            }
                                        }
                                        break;
                                    case 2:                                        
                                        startPoint = newVerts[vertIndices[currentNode.BotLeftVertIndex]].y;
                                        endPoint = newVerts[vertIndices[currentNode.TopLeftVertIndex]].y;

                                        // go through each inner square and position the corners
                                        for (int j = 1; j < VertsPerSide - 1; j++) {
                                            for (int k = 0; k < VertsPerSide; k++) {
                                                int currentIndex = j * rowSize + k;
                                                newVerts[vertIndices[currentIndex]].y = Mathf.Lerp(startPoint, endPoint, newVerts[vertIndices[currentIndex]].z % 1f);
                                            }
                                        }
                                        break;
                                    case 3:
                                        
                                        startPoint = newVerts[vertIndices[currentNode.BotLeftVertIndex]].y;
                                        endPoint = newVerts[vertIndices[currentNode.BotRightVertIndex]].y;

                                        // go through each inner square and position the corners
                                        for (int j = 0; j < VertsPerSide; j++) {
                                            for (int k = 1; k < VertsPerSide - 1; k++) {
                                                int currentIndex = j * rowSize + k;
                                                newVerts[vertIndices[currentIndex]].y = Mathf.Lerp(startPoint, endPoint, newVerts[vertIndices[currentIndex]].x % 1f);
                                            }
                                        }

                                        break;
                                }

                                // go through each inner square and position the center points
                                for (int j = 0; j < VertsPerSide - 1; j++) {
                                    for (int k = 0; k < VertsPerSide - 1; k++) {
                                        int botLeftIndex = j * rowSize + k;
                                        float botLeftHeight = newVerts[vertIndices[botLeftIndex]].y;
                                        int topRightIndex = j * rowSize + k + rowSize + 1;
                                        float topRightHeight = newVerts[vertIndices[topRightIndex]].y;
                                        int centerPointIndex = j * rowSize + k + VertsPerSide;
                                        newVerts[vertIndices[centerPointIndex]].y = Mathf.Lerp(botLeftHeight, topRightHeight, 0.5f);
                                    }
                                }


                            }
                        }

                    }


                    // @todo setup inner corner ramps -- 2 sides adjacent to 2 higher levels
                    for (int i = 0; i < neighborNodes.Length; i++) {
                        GeoNode neighborNode = neighborNodes[i];
                        float height0, height1;
                        if (neighborNode != null) {
                            switch (i) {

                                case 0:

                                    /*
                                    bool leftTest = neighborNodes[1] == null || neighborNodes[1].Height % 1 == 0.5f;
                                    bool rightTest = neighborNodes[3] == null || neighborNodes[3].Height % 1 == 0.5f;

                                    if (neighborNode.Height % 1 == 0 && leftTest && rightTest) {

                                        height0 = newVerts[indices[currentNode.BotLeftVertIndex]].y;
                                        height1 = newVerts[indices[currentNode.TopLeftVertIndex]].y;

                                        for (int j = 0; j < VertsPerNode; j++) {

                                            if (newVerts[indices[j]].z % 1 != 0f) {
                                                float currentHeight = newVerts[indices[j]].y;
                                                float newHeight = Mathf.Lerp(height0, height1, newVerts[indices[j]].z % 1);
                                                newVerts[indices[j]].y = newHeight;
                                            }

                                        }
                                    }
                                    */

                                    //break;

                                //case 1:

                                    /*
                                    bool bottomTest = neighborNodes[0] == null || neighborNodes[0].Height % 1 == 0.5f;
                                    bool topTest = neighborNodes[2] == null || neighborNodes[2].Height % 1 == 0.5f;


                                    if (x == 1 && z == 1) {
                                        //Debug.Log(neighborNode.Height % 1 == 0);
                                        //Debug.Log(bottomTest);
                                        //Debug.Log(topTest);
                                    }

                                    if (neighborNode.Height % 1 == 0 && bottomTest && topTest) {

                                        height0 = newVerts[vertIndices[currentNode.BotLeftVertIndex]].y;
                                        height1 = newVerts[vertIndices[currentNode.BotRightVertIndex]].y;

                                        for (int j = 0; j < VertsPerNode; j++) {

                                            if (newVerts[vertIndices[j]].x % 1 != 0f) {
                                                float currentHeight = newVerts[vertIndices[j]].y;
                                                float newHeight = Mathf.Lerp(height0, height1, newVerts[vertIndices[j]].x % 1);
                                                newVerts[vertIndices[j]].y = newHeight;
                                            }

                                        }
                                    } else {

                                        Debug.Log(nodes[x, z]);

                                        for (int k = 0; k < currentNode.VertIndices.Length; k++) {

                                            Vector3 currentVertex = newVerts[vertIndices[k]];
                                            float currentVertX = currentVertex.x;
                                            float currentVertZ = currentVertex.z;

                                            float startHeight, endHeight;

                                            bool bottomRow = k < VertsPerSide;
                                            bool topRow = k >= VertsPerNode - VertsPerSide;
                                            bool leftColumn = k % (2 * VertsPerSide - 1) == 0;
                                            bool rightColumn = (k - VertsPerSide + 1) % (2 * VertsPerSide - 1) == 0;

                                            if (currentVertX > currentVertZ) {

                                                if (!bottomRow && !topRow && !leftColumn && !rightColumn) {
                                                    newVerts[vertIndices[k]].y = Mathf.Floor(nodeHeight) * heightFactor;
                                                }
                                                /*
                                                startHeight = newVerts[indices[currentNode.BotLeftVertIndex]].y;
                                                endHeight = newVerts[indices[currentNode.BotRightVertIndex]].y;
                                                float newHeight = Mathf.Lerp(startHeight, endHeight, newVerts[indices[k]].x % 1);
                                                newVerts[indices[k]].y = newHeight;
                                                */
                                    /*
                                            }

                                        }

                                    }
                                    break;
                                    */
                                    /*
                            }

                        }
                    }
                    */
                    // bot side vertex lerp
                    /*
                    float startPoint = newVerts[indices[currentNode.BotLeftVertIndex]].y;
                    float endPoint = newVerts[indices[currentNode.BotRightVertIndex]].y;
                    Debug.Log(currentNode);
                    Debug.Log(startPoint);
                    Debug.Log(endPoint);
                    for (int i = 0; i < currentNode.BotSideVertIndices.Length; i++) {
                        //newVerts[currentNode.BotSideVertIndices[i]].y = 
                    }
                    */

                    // left side vertex lerp

                    // top side vertex lerp

                    // right side vertex lerp

                    /*
                    for (int i = 0; i < currentNode.VertIndices.Length; i++) {

                        Vector3 currentVertex = newVerts[indices[i]];
                        float currentVertX = currentVertex.x;
                        float currentVertZ = currentVertex.z;

                        float startHeight, endHeight;

                        if (currentVertX > currentVertZ) {

                            startHeight = newVerts[indices[currentNode.BotLeftVertIndex]].y;
                            endHeight = newVerts[indices[currentNode.BotRightVertIndex]].y;
                            float newHeight = Mathf.Lerp(startHeight, endHeight, newVerts[indices[i]].x % 1);
                            newVerts[indices[i]].y = newHeight;

                        }

                    }

                    
                }

                // corners
                /*
                if (nodeHeight % 1 == 0.5f && neighborLevels.Count == 1) {                    

                    for (int i = 0; i < currentNode.VertIndices.Length; i++) {

                        bool bottomRow = i < VertsPerSide;
                        bool topRow = i >= VertsPerNode - VertsPerSide;
                        bool leftColumn = i % (2 * VertsPerSide - 1) == 0;
                        bool rightColumn = (i - VertsPerSide + 1) % (2 * VertsPerSide - 1) == 0;


                        if (!bottomRow && !topRow && !leftColumn && !rightColumn) {
                            newVerts[indices[i]].y = Mathf.Floor(nodeHeight) * heightFactor;
                        } 

                    }

                }

                // set color for texture
                for (int i = 0; i < MapGenerator.Terrains.Count; i++) {
                    TerrainData terrainData = MapGenerator.Terrains[i];
                    if (GeoMap.access.NormalizedHeightMap[x, z] <= terrainData.MaxHeight) {
                        colors[z * MapWidth + x] = terrainData.Color;
                        break;
                    }
                    */
                    /*
                }
            }
        }


        mesh.vertices = newVerts;
        */

        // set texture for mesh
        // set color for texture
        /*
        for (int i = 0; i < MapGenerator.Terrains.Count; i++) {
            TerrainData terrainData = MapGenerator.Terrains[i];
            if (GeoMap.access.NormalizedHeightMap[x, z] <= terrainData.MaxHeight) {
                colors[z * MapWidth + x] = terrainData.Color;
                break;
            }
        }
        */

        texture.SetPixels(colors);
        texture.Apply();

        MeshRenderer.sharedMaterial.mainTexture = texture; 
    }

    //
    private void UpdateMeshCollider() {
        GetComponent<MeshCollider>().sharedMesh = MeshFilter.mesh;
    }

    //
    public void DrawGridLines() {

        /*
        GeoNode[,] tiles = GeoMap.access.Nodes;

        DestroyGridLines();

        GeoNode tempTile;
        Mesh mesh = MeshFilter.mesh;
        
        int numVectors = MapWidth * (VertsPerSide - 1) + 1;
        Vector3[] positions = new Vector3[numVectors];
        int[] currentTileVertices;
        int vertIndex;
        Material gridLineMaterial = (Material)Resources.Load("Grid Lines");

        // horizontal lines
        for (int z = 0; z < MapHeight; z++) {
            vertIndex = 0;

            GameObject Line = new GameObject();
            Line.transform.parent = gridLines.transform;
            Line.transform.position = Line.transform.position + new Vector3(0, 0, 0.5f);
            LineRenderer line = Line.AddComponent<LineRenderer>();
            line.startWidth = lineWidth;
            line.endWidth = lineWidth;
            line.material = gridLineMaterial;
            line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            line.alignment = LineAlignment.TransformZ;

            for (int x = 0; x < MapWidth; x++) {
                tempTile = tiles[x, z];
                currentTileVertices = tempTile.BotSideVertIndices;

                for (int i = 1; i < currentTileVertices.Length; i++) {
                    int currentVertIndex = currentTileVertices[i];

                    if (x == 0 && i == 1) { // add the bot left vertex of the first node first
                        positions[vertIndex] = mesh.vertices[currentTileVertices[0]] + new Vector3(0, 0.01f, 0);
                        vertIndex++;
                    }

                    positions[vertIndex] = mesh.vertices[currentVertIndex] + new Vector3(0, 0.01f, 0);
                    vertIndex++;
                }
            }

            line.positionCount = numVectors;
            line.SetPositions(positions);
        }

        // vertical lines
        for (int x = 0; x < MapWidth; x++) {
            vertIndex = 0;

            GameObject Line = new GameObject();
            Line.transform.parent = gridLines.transform;
            LineRenderer line = Line.AddComponent<LineRenderer>();
            line.startWidth = lineWidth;
            line.endWidth = lineWidth;
            line.material = gridLineMaterial;
            line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            //line.alignment = LineAlignment.Local;

            for (int z = 0; z < MapHeight; z++) {
                tempTile = tiles[x, z];
                currentTileVertices = tempTile.LeftSideVertIndices;

                for (int i = 1; i < currentTileVertices.Length; i++) {
                    int currentVertIndex = currentTileVertices[i];

                    if (z == 0 && i == 1) { // add the bot left vertex of the first node first
                        positions[vertIndex] = mesh.vertices[currentTileVertices[0]] + new Vector3(0, 0.01f, 0);
                        vertIndex++;
                    }

                    positions[vertIndex] = mesh.vertices[currentVertIndex] + new Vector3(0, 0.01f, 0);
                    vertIndex++;
                }
            }

            line.positionCount = numVectors;
            line.SetPositions(positions);
        }
        */
    }

    // should be redone to modify grid lines rather than destroying them
    public void DestroyGridLines() {
        var children = new List<GameObject>();
        foreach (Transform child in gridLines.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

    //
    public void ShowLabels() {

        if (EnableLabels) {
            //WorldUI.access.GeoMapLabels.ShowLabels(GeoViewLabelType, GeoMap.access.Nodes);
        }

    }
}

public class MapNodeView {

    public Tile Tile;

    public int[] vertices { get; set; }
    public int[] VertIndices { get { return vertices; } set { vertices = value; } }
    public int[] RightSideVertIndices { get; set; }
    public int[] TopSideVertIndices { get; set; }
    public int[] BotSideVertIndices { get { return new int[5] { VertIndices[0], VertIndices[1], VertIndices[2], VertIndices[3], VertIndices[4] }; } }
    public int[] LeftSideVertIndices { get { return new int[5] { VertIndices[0], VertIndices[9], VertIndices[18], VertIndices[27], VertIndices[36] }; } }

    public int BotLeftVertIndex { get { return 0; } }
    public int BotRightVertIndex { get { return 4; } }
    public int TopLeftVertIndex { get { return 36; } }
    public int TopRightVertIndex { get { return 40; } }
    public int CenterVertIndex { get { return 20; } }

    public int[] TLBRdiagonal { get { return new int[4] { 20, 16, 8, 4 }; } }
    public int[] BLTRdiagonal { get { return new int[4] { 0, 6, 18, 24 }; } }

    public int[] BottomTriangle { get { return new int[4] { 1, 2, 3, 7 }; } }
    public int[] LeftTriangle { get { return new int[4] { 5, 10, 11, 15 }; } }
    public int[] TopTriangle { get { return new int[4] { 17, 21, 22, 23 }; } }
    public int[] RightTriangle { get { return new int[4] { 9, 13, 14, 19 }; } }

    // get exact vector3 positions of vertices based on position in tile
    /*
    public MapPoint BotLeftPoint { get { return new MapPoint(GeoView.access.MeshFilter.mesh.vertices[VertIndices[BotLeftVertIndex]]); } }
    public MapPoint BotRightPoint { get { return new MapPoint(GeoView.access.MeshFilter.mesh.vertices[VertIndices[BotRightVertIndex]]); } }
    public MapPoint TopLeftPoint { get { return new MapPoint(GeoView.access.MeshFilter.mesh.vertices[VertIndices[TopLeftVertIndex]]); } }
    public MapPoint TopRightPoint { get { return new MapPoint(GeoView.access.MeshFilter.mesh.vertices[VertIndices[TopRightVertIndex]]); } }
    public MapPoint CenterPoint {
        get {
            if (nullNode) {
                return new MapPoint(x + 0.5f, z + 0.5f);
            }
            return new MapPoint(GeoView.access.MeshFilter.mesh.vertices[VertIndices[CenterVertIndex]]);
        }
    }
    */

    public MapNodeView(Tile node) {
        Tile = node;
    }


    //
    public void SetVertIndices(int[] vertIndices) {
        VertIndices = vertIndices;
    }

}
