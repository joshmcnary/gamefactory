﻿using System;
using UnityEngine;

// managers add an extra layer of control over normal monobehaviours by enforcing a structured loading procedure
// managers are loaded by world in a specific order.. init->reset->loadsettings->run

[Serializable]
public class Manager : MonoBehaviour {

    public WorldSettings settings;

    public virtual void Init() { }                                  // one-time calls
    public virtual void Reset() { }                                 // startup/reset calls, need to happen every new/load game

    // load the settings the world has instantiated and use those to do stuff
    public virtual void LoadSettings(WorldSettings _settings) {
        settings = _settings;
    }

    // we know all managers are loaded, now we can do anything that requires talking to other managers
    public virtual void Run() { }
}
