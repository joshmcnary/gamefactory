﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;

/*
    used to manage the state of the game

    SetGameState(GameState.Default) resets state, this is how state should be reset
*/

public enum HighlightMode { None, Map, Entity, UI }
public enum GameState { Default, Build, Place, Fire, Transition }

public class WorldStateManager : NetworkBehaviour {

    public static WorldStateManager access;

    [SyncVar]
    public int MessagesCount = 1;
    [SyncVar]
    public int MessagesRate = 120;

    // game timer
    public float GameTimer { get; private set; }
    public event Action<float> GameSecondsTick;
    public bool Paused = false;

    // state timer
    public float StateTimer { get; private set; }
    public float StateTimeLeft { get; private set; }
    public event Action<float> StateSecondsTick;
    public float TransitionTime { get; private set; }

    public event System.Action<TileOld> UpdateHighlightedMapNode;
    //public event WorldStateEventHandler EntitySelected;

    [Header("Highlighting")]
    public bool highlightingEnabled = true;
    public HighlightMode highlightMode = HighlightMode.None;
    public TileOld highlightedTile { get; set; }
    public PhysicsRaycaster raycaster { get; private set; }
    public PointerEventData pointerData { get; private set; }
    public List<RaycastResult> results { get; private set; }

    [Header("Selection")]
    public GameState GameState;
    public event Action<GameState> StateChange;
    /*
    Entity selectedEntity;
    public Entity SelectedEntity {
        get { return selectedEntity; }
        set {
            selectedEntity = value;
        }
    }
    public GeoNode selectedGeoNode = null;
    */
    
    [Header("Building")]
    public EntityType activeEntityTypeToBuild;
    public event Action<EntityType> UpdateEntityToBuild;
    /*
    public Build activeBuild { get; set; }
    public bool hasCost { get; set; }
    public ResourceGroup BuildingResourcesInProcess { get; private set; }
    private Material validBuildMaterial { get; set; }
    private Material invalidBuildMaterial { get; set; }
    */

    /*
    [Header("Cursor")]
    public Texture2D SelectCursor;
    public Texture2D AssignWorkerCursor;
    public Texture2D TransferWorkerCursor;
    public Texture2D BuildCursor;
    public Texture2D SellCursor;

    [Header("Global Abilities")]
    public List<AbilitySettings> AbilitySettings;
    public AbilityList AbilityList { get; private set; }
    public Ability ActiveAbility { get; private set; }
    public List<TargetType> ActiveAbilityValidTargetTypes { get; private set; }
    public List<Target> ActiveAbilityTargets { get; private set; }
    */

    public List<Room> Rooms;
    public event Action<Room> RoomAdded;
    public event Action<List<Room>> RoomsUpdated;    

    //
    public void Init() {

        pointerData = new PointerEventData(EventSystem.current);
        raycaster = Camera.main.GetComponent<PhysicsRaycaster>();
        results = new List<RaycastResult>();
        /*
        validBuildMaterial = Resources.Load("Green Transparent") as Material;
        invalidBuildMaterial = Resources.Load("Red Transparent") as Material;

        //SetLeftClickMode(LeftClickMode.Select);

        // init global abilities
        AbilityList = GetComponent<AbilityList>();
        AbilityList.CreateAbilitiesFromSettings(AbilitySettings);
        */

        World.access.GridView.TileLeftPressed += delegate (Tile tile) { LeftClickTile(tile); };

        TransitionTime = 1;
    }

    // 
    public void Reset() {

        GameTimer = 0;

        SetTime(1);

        SetGameState(GameState.Default);

        Rooms = new List<Room>();
    }

    //
    public void Run() {

        /*
        UI.access.Conditions.SetGameTimer((int)GameTimer);
        UI.access.Conditions.SetDisasterTimer((int)DisasterTimer);
        Resume();
        */

    }

    void FixedUpdate() {

        // game timer
        float lastGameTime = GameTimer;
        GameTimer += Time.deltaTime;
        if (Mathf.Floor(lastGameTime) != Mathf.Floor(GameTimer)) {
            OnGameSecondsTick();
        }

        // state timer
        float lastStateTime = StateTimer;
        if (StateTimer > 0) { StateTimer -= Time.deltaTime; }
        if (Mathf.Floor(lastStateTime) != Mathf.Floor(StateTimer)) {
            OnStateSecondsTick();
        }
        if (StateTimer <= 0 && GameState != GameState.Transition) {
            switch (GameState) {
                case GameState.Build: StartCoroutine(TransitionToState(TransitionTime, GameState.Place)); break;
                case GameState.Place: StartCoroutine(TransitionToState(TransitionTime, GameState.Fire)); break;
                case GameState.Fire: StartCoroutine(TransitionToState(TransitionTime, GameState.Build)); break;
            }
        }
    }
	
	//
	void Update () {

        /*
        if (Input.GetMouseButtonDown(0)) {
            //Debug.Log(MapData.access.Width);

            var players = FindObjectsOfType(typeof(GamePlayer)) as GamePlayer[];
            if (players.Length == 0) { return; }
            foreach (var player in players) {
                if (player.GetComponent<NetworkIdentity>().hasAuthority) {
                    player.CmdSetTile();
                }
            }
        }
        */

        // keep camera inside bounds of map
        results.Clear();
        pointerData.position = Camera.main.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        raycaster.Raycast(pointerData, results);
        if (results.Count == 0) {
            World.access.Camera.transform.position = World.access.Camera.LastValidPosition;
        }

        if (isClientOnly) {
            Debug.Log(0);
        }

        // highlighting
        if (highlightingEnabled) {

            pointerData.position = Input.mousePosition;
            results.Clear();

            raycaster.Raycast(pointerData, results);

            for (int i = 0; i < results.Count; i++) {

                if (isClientOnly) {
                    Debug.Log(1);
                }
                RaycastResult result = results[i];
                if (result.gameObject.name != "GridView") {
                    continue;
                } else {

                    //Tile currentTile = GridData.access.GetTileNew(new Point(result.worldPosition));

                    /*
                    if (highlightedTile != currentNode && highlightMode != HighlightMode.UI) {
                        //SetHighlightedMapNode(currentNode);
                    }
                    */

                    var players = FindObjectsOfType(typeof(GamePlayer)) as GamePlayer[];
                    if (players.Length == 0) { return; }
                    foreach (var player in players) {
                        if (player.GetComponent<NetworkIdentity>().hasAuthority) {
                            player.transform.position = result.worldPosition;
                        }
                    }
                }
                if (isClientOnly) {
                    Debug.Log(2 + i);
                }
            }

            
        }

        //
        /*
        if (LeftClickMode == LeftClickMode.BuildStructure || LeftClickMode == LeftClickMode.BuildPath) {

            if (activeBuild.entity == null) {

                SetLeftClickMode(LeftClickMode.Default, false);
                UI.access.Bottom.buildListContainer.Hide();

            } else {

                float scroll = Input.GetAxis("Mouse ScrollWheel");
                if (scroll != 0) {
                    int rotations = (int)(scroll / 0.1f);
                    activeEntityToBuild.StandardModule.Blockable.RotateBlockable(rotations);
                    bool buildIsValid = activeBuild.Validate();

                    if (buildIsValid) {
                        activeEntityToBuild.SetMaterial(Resources.Load("Green Transparent") as Material);
                    }
                    else {
                        activeEntityToBuild.SetMaterial(Resources.Load("Red Transparent") as Material);
                    }
                }

            }

            
        }
        */

        // detect right click
        if (Input.GetMouseButtonDown(1) && highlightMode == HighlightMode.None) {
            //UI.access.ResetSelection();
        }
    }

    //
    void LeftClickTile(Tile tile) {

        // change clicked tile to grass
        var players = FindObjectsOfType(typeof(GamePlayer)) as GamePlayer[];
        if (players.Length == 0) { return; }
        foreach (var player in players) {
            if (player.GetComponent<NetworkIdentity>().hasAuthority) {

                // change clicked tile to grass
                //player.CmdSetTile((int)node.coords.x, (int)node.coords.z);

                // add or remove wall depending on whether or not there is one already in tile
                player.CmdCreateEntity(EntityType.Wall, tile);
            }
        }

        return;
        switch (GameState) {

            case GameState.Build:

                Entity effectEntity = GridEffects.access.Wall;
                List<Tile> blockedNodes = effectEntity.GetComponent<Blockable>().Tiles;
                if (effectEntity.GetComponent<Blockable>().IsOnValidPosition) {

                    // build a wall on each blocked tile.. this can be changed later maybe
                    for (int i = 0; i < blockedNodes.Count; i++) {
                        Tile blockedNode = blockedNodes[i];
                        //EntityManager.access.AddNewEntityToGame(EntityType.Wall, blockedNode.origin);
                    }

                    // detect rooms
                    //MapData.access.InitializeFloodFillAlgorithm(blockedNodes[0]);


                    // set next entity to build

                    /*
                    if (entityToBuild.entityType == EntityType.WallPlus) {
                        MapEffects.access.Wall = EntityManager.access.CreateEntity(EntityType.Wall);
                    } else {
                        MapEffects.access.Wall = EntityManager.access.CreateEntity(EntityType.Wall);
                    }
                    EntityManager.access.DestroyEntity(entityToBuild);
                    */

                    //SetHighlightedMapNode(highlightedMapNode);

                } else {
                    Debug.Log("need to handle case where build is invalid");
                }
                

            break;

            case GameState.Place:

                effectEntity = GridEffects.access.Cannon;
                blockedNodes = effectEntity.GetComponent<Blockable>().Tiles;
                if (effectEntity.GetComponent<Blockable>().IsOnValidPosition) {
                    EntityManager.access.AddNewEntityToGame(EntityType.Cannon, highlightedTile.origin);
                }
                else {
                    Debug.Log("need to handle case where build is invalid");
                }


                break;

            case GameState.Fire:

                Entity wallToDestroy = highlightedTile.blockableEntity;
                if (wallToDestroy == null) { return; }
                if (wallToDestroy.entityType != EntityType.Wall) { return; }

                EntityManager.access.RemoveAndDestroyEntity(wallToDestroy);                

                break;
        }

    }

    //
    public static void SetTime(float timeScale) {
        Time.timeScale = timeScale;
    }

    // pauses the current game
    public void Pause() {

        Paused = true;
        SetTime(0);

    }

    //
    public void Resume() {

        Paused = false;
        SetTime(1);

    }

    //
    public void Win() {

        Pause();

        //UI.access.EndMenu.Show();
        //UI.access.EndMenu.SetLabel("You Win!");

    }

    //
    public void Lose(string lossText) {

        //if (World.access.Settings.DisableWinLoss) { return; }

        StartCoroutine(LoseSequence(lossText));

    }

    IEnumerator LoseSequence(string lossText) {

        yield return new WaitForSeconds(2f);

        Pause();

        //UI.access.EndMenu.Show();
        //UI.access.EndMenu.SetLabel("You Lose!");
        //UI.access.EndMenu.SetEndingText(lossText);

    }

    //
    /*
    public void ClickEntity(Entity entity) {

        switch (LeftClickMode) {

            case LeftClickMode.Default:
                SelectEntity(entity);
                break;

            case LeftClickMode.Ability:

                TargetType targetType = TargetType.Gaean;
                switch (entity.entityClass) {

                    case EntityClass.Human: targetType = TargetType.Gaean; break;

                    case EntityClass.Structure:
                    case EntityClass.Path:
                        targetType = TargetType.Structure;
                        break;

                    case EntityClass.Machine: targetType = TargetType.Machine; break;

                }
                AddAbilityTarget(new Target(targetType, entity));

                break;

            case LeftClickMode.AssignWorker:

                // either gaean or housing will be selected.. this will make sure we have reference to the gaean we want to assign
                StoreHuman selectedHousing = (StoreHuman)SelectedEntity.GetModule(typeof(StoreHuman));
                Entity selectedGaean = null;
                if (SelectedEntity.entityClass == EntityClass.Human) {
                    selectedGaean = SelectedEntity;
                }
                else if (selectedHousing != null) {
                    selectedGaean = UI.access.Bottom.StoreHumanContainer.ActiveHuman;
                }

                Worker worker = (Worker)selectedGaean.GetModule(typeof(Worker));
                Entity workplace = selectedGaean.GetComponent<Worker>().CurrentWorkplace;
                if (workplace != null) {
                    workplace.GetComponent<StoreHuman>().UnhouseHuman(selectedGaean);
                    worker.Working = false;
                    worker.BackToWorkRequest -= worker.BackToWorkDelegate;
                }

                StoreHuman workplaceToAssignTo = entity.GetComponent<StoreHuman>();
                if (workplaceToAssignTo != null) {
                    
                    if (workplaceToAssignTo.Settings.storeHumanType == StoreHumanType.Commercial) {

                        workplaceToAssignTo.AddHuman(selectedGaean);

                        worker.BackToWorkDelegate = delegate { workplaceToAssignTo.MoveHumanToHouse(worker.entity); };
                        worker.BackToWorkRequest += worker.BackToWorkDelegate;

                        if (!worker.Priorities.Exists(e => e.Type == PriorityType.Work)) {
                            worker.AddPriority(new WorkerPriority(PriorityType.Work, 0));
                        } else {
                            worker.SetCurrentPriorityType(PriorityType.Work);
                        }

                    } else {
                        UI.access.warningDisplay.AddWarning("Need to assign worker to a place where they can work");
                    }
                } else {
                    UI.access.warningDisplay.AddWarning("This is not a valid work location");
                }

                break;

            case LeftClickMode.TransferWorker:

                StoreHuman housingToTransferTo = entity.GetComponent<StoreHuman>();
                if (housingToTransferTo != null) {

                    if (housingToTransferTo.Settings.storeHumanType == StoreHumanType.Residential) {
                        selectedHousing = (StoreHuman)SelectedEntity.GetModule(typeof(StoreHuman));

                        Entity selectedHuman = null;
                        if (SelectedEntity.entityClass == EntityClass.Human) {
                            selectedHuman = SelectedEntity;
                            selectedHousing = selectedHuman.GetComponent<Worker>().CurrentHousing;
                        }
                        else if (selectedHousing != null) {
                            selectedHuman = UI.access.Bottom.StoreHumanContainer.ActiveHuman;
                        }

                        selectedHousing.TransferHuman(selectedHuman, housingToTransferTo);
                    }
                    else {
                        UI.access.warningDisplay.AddWarning("Need to assign worker to a place where they can live");
                    }
                }
                else {
                    UI.access.warningDisplay.AddWarning("This is not a valid housing location");
                }

                break;

            case LeftClickMode.Sell:

                if (entity.entityClass != EntityClass.Structure) {
                    return;
                }

                ResourceGroup resourcesGained = entity.StandardModule.Settings.Cost;
                resourcesGained.Scale(0.5f);
                ResourceManager.access.AddPlayerResources(0, resourcesGained);

                EntityManager.access.DestroyEntity(entity);

                UI.access.warningDisplay.AddWarning(entity.entityType + " was sold");

                break;

        }

    }

    //
    public void SelectEntity(Entity entity) {

        selectedGeoNode = null;
        SelectedEntity = entity;

        // entity selected event
        WorldStateEventArgs args = new WorldStateEventArgs();
        args.entitySelected = SelectedEntity;
        OnEntitySelected(args);
    }
    */

    //
    public void SetGameState(GameState state, bool resetSelectedEntity = true) {

        GameState = state;

        switch (state) {

            case GameState.Default:

                //Cursor.SetCursor(SelectCursor, Vector2.zero, CursorMode.Auto);
                //ResetState(resetSelectedEntity);
                break;

            case GameState.Build:

                StateTimer = 5;

                // detect rooms that still exist and update effects
                UpdateRooms();

                // set entity to build
                activeEntityTypeToBuild = EntityType.Wall;
                OnUpdateEntityToBuild();

                // make it so only mapview detects clicks
                raycaster.eventMask = LayerMask.GetMask(new string[1] { "MapView" });

                break;

            case GameState.Place:

                StateTimer = 5;
                OnStateSecondsTick();

                break;

            case GameState.Fire:

                StateTimer = 5;
                OnStateSecondsTick();

                break;

                /*
            case LeftClickMode.AssignWorker: Cursor.SetCursor(AssignWorkerCursor, Vector2.zero, CursorMode.Auto); break;
            case LeftClickMode.TransferWorker: Cursor.SetCursor(TransferWorkerCursor, Vector2.zero, CursorMode.Auto); break;
            case LeftClickMode.BuildMode: Cursor.SetCursor(BuildCursor, Vector2.zero, CursorMode.Auto); break;
            case LeftClickMode.Sell: Cursor.SetCursor(SellCursor, Vector2.zero, CursorMode.Auto); break;
            */

        }

        OnStateChange();
    }

    //
    IEnumerator TransitionToState(float seconds, GameState state) {

        SetGameState(GameState.Transition);
        yield return new WaitForSeconds(seconds);
        SetGameState(state);

    }

    //
    /*
    public void RightClick(HighlightMode highlightMode, Vector3 scaledPosition, Entity clickedEntity) {

        if (SelectedEntity != null) {

            LeftClickMode clickMode = LeftClickMode;

            if (clickMode == LeftClickMode.Default) {

                Movable movable = SelectedEntity.GetComponent<Movable>();
                if (movable != null && highlightMode == HighlightMode.Map) {
                    if (!Input.GetKey(KeyCode.RightShift)) {
                        movable.ClickMove(GeoMap.access.GetGeoNode(scaledPosition));
                    }
                    else {
                        movable.ClickMove(GeoMap.access.GetGeoNode(scaledPosition), false);
                    }

                }
                else {

                    GeoLayers.access.RemoveEffects();
                    GeoLayers.access.RemoveStateEffects();
                    SetLeftClickMode(LeftClickMode.Default);
                    //ResetState();
                    UI.access.ResetSelection();

                }
            }
            else if (clickMode == LeftClickMode.AssignWorker || clickMode == LeftClickMode.TransferWorker) {

                SetLeftClickMode(LeftClickMode.Default);
                GeoLayers.access.RemoveEffects();
                GeoLayers.access.RemoveStateEffects();
                if (activeEntityToBuild != null) {
                    DestroyActiveEntityToBuild();
                }

            }
            else if (clickMode == LeftClickMode.BuildMode || clickMode == LeftClickMode.BuildStructure) {

                Entity reselectEntity = selectedEntity;

                SetLeftClickMode(LeftClickMode.Default);
                GeoLayers.access.RemoveEffects();
                GeoLayers.access.RemoveStateEffects();
                if (activeEntityToBuild != null) {
                    DestroyActiveEntityToBuild();
                }

                if (clickMode == LeftClickMode.BuildMode) {
                    activeBuild = null;
                }

                UI.access.Bottom.buildListContainer.Hide();

                SelectEntity(reselectEntity);


            } else if (clickMode == LeftClickMode.BuildPath) {

                Build build = SelectedEntity.StandardModule.AbilityList.GetAbility(typeof(Build)) as Build;
                
                if (build.PathNodesBeingBuilt != null && !activeEntityToBuild.GetComponent<Path>().IsGate) {
                    if (build.PathNodesBeingBuilt.Count >= 2 && !activeEntityToBuild.GetComponent<Path>().IsGate) {
                        build.RemovePathWaypoint();
                    } else {
                        SetLeftClickMode(LeftClickMode.Default, false);
                    }
                } else {
                    SetLeftClickMode(LeftClickMode.Default, false);

                    UI.access.Bottom.buildListContainer.Hide();
                }

            } else if (clickMode == LeftClickMode.Ability) {

                if (ActiveAbilityTargets.Count >= 1) {
                    ActiveAbilityTargets.RemoveAt(ActiveAbilityTargets.Count - 1);
                } else {
                    SetLeftClickMode(LeftClickMode.Default);
                }

            }

        }
        else {
            UI.access.ResetSelection();
            SetLeftClickMode(LeftClickMode.Default);
            GeoLayers.access.RemoveEffects();
            GeoLayers.access.RemoveStateEffects();
        }

    }

    // shortcut for right click for entities
    /*
    public void RightClick(Entity entity) {
        RightClick(HighlightMode.Entity, new Vector3(-100, 0, 0), entity);
    }
    */


    //
    public void SetHighlightMode(HighlightMode _hlmode) {

        highlightMode = _hlmode;

        switch (highlightMode) {

            /*

            case HighlightMode.UI:

                if (activeEntityToBuild != null) {
                    activeEntityToBuild.Move(GeoMap.offMapPoint);
                }

                break;
                */

        }
    }

    // resets the state back to its default state
    void ResetState(bool resetSelectedEntity = true) {

        /*
        if (resetSelectedEntity) {
            //SelectedEntity = null;
            //UI.access.Bottom.ResetSelection();
        }
        GeoLayers.access.RemoveEffects();
        GeoLayers.access.RemoveStateEffects();

        if (activeEntityToBuild != null) {
            DestroyActiveEntityToBuild();
        }
        */

    }

    // create a build entity for selecting where to build
    /*
    public void SetActiveEntityToBuild(EntityType entityType) {
        activeEntityTypeToBuild = entityType;        
        activeEntityToBuild = World.access.entityManager.CreateEntity(entityType, false);
    }
    
    // destroys the build entity
    public void DestroyActiveEntityToBuild() {
        Destroy(activeEntityToBuild.gameObject);
        activeEntityToBuild = null;
    }
    */

    // runs whenever players supply resources are updated
    /*
    public void ResourcesUpdate(ResourceGroup resources) {

        // validate enough resources exist when building
        if (activeEntityToBuild == null) { return; }

        // get cost to build entity
        List<Resource> costList = World.access.entityManager.DefaultEntities[activeEntityTypeToBuild].defaultSettings.standardSettings.CostList;
        ResourceGroup cost = new ResourceGroup(costList).Clone();
        resources = resources.Clone();
        resources = resources.RemoveResources(BuildingResourcesInProcess);

        // set material of build entity based on whether player has enough resources
        if (resources.HasResources(cost)) {
            hasCost = true;
            SetActiveEntityMaterial(true);
        } else {
            hasCost = false;
            SetActiveEntityMaterial(false);
        }

    }
    */

    //
    /*
    public void ValidateActiveBuild() {

        Build build = activeBuild;

        build.ValidateBuildRegion();
        ValidateBuildingCost();

    }

    //
    public void ValidateBuildingCost() {

        List<Resource> costList = World.access.entityManager.DefaultEntities[activeEntityTypeToBuild].defaultSettings.standardSettings.CostList;
        ResourceGroup cost = new ResourceGroup(costList).Clone();
        ResourceGroup resources = ResourceManager.access.GetPlayerResources(0).Clone().RemoveResources(BuildingResourcesInProcess);

        if (resources.HasResources(cost)) {
            hasCost = true;
            if (LeftClickMode == LeftClickMode.BuildStructure) {
                SetActiveEntityMaterial(true);
            } else if (LeftClickMode == LeftClickMode.BuildPath) {
                activeEntityToBuild.SetMaterial(Resources.Load("Yellow Transparent") as Material);
            }
        }
        else {
            hasCost = false;
            SetActiveEntityMaterial(false);
        }

    }

    // makes the build entity green if true, and red if not
    /*
    private void SetActiveEntityMaterial(bool active) {
        if (active && activeBuild.buildRegionValid) {
            activeEntityToBuild.SetMaterial(validBuildMaterial);
        }
        else {
            activeEntityToBuild.SetMaterial(invalidBuildMaterial);
        }
    }
    */

    //
    /*
    public void ConfirmActiveBuild() {

        activeBuild.BuildStructure(
            activeEntityTypeToBuild.ToString(),
            new List<GeoNode> { highlightedGeoNode },
            activeEntityToBuild.transform.rotation.eulerAngles.y
        );

        SetLeftClickMode(LeftClickMode.Default, false);

    }
    */

    //
    public void SetHighlightedMapNode(TileOld node) {

        highlightedTile = node;


        

        //
        /*
        GeoLayers geoLayers = GeoLayers.access;
        GeoMap geoMap = GeoMap.access;

        // if highlighting the ui, ignore the geonode change
        if (highlightMode == HighlightMode.UI) { return; }

        // moves active entity to build as different regions are highlighted
        if (LeftClickMode == LeftClickMode.BuildStructure || LeftClickMode == LeftClickMode.BuildPath) {
            try {
                activeEntityToBuild.GetComponent<Blockable>().MoveBlockable(highlightedGeoNode.BotLeftPoint);
            }
            catch (System.Exception e) {
                Debug.Log(e);
            }
        }

        if (isBuilding) {

            Entity buildingEntity = SelectedEntity;
            AbilityList abilityList = buildingEntity.GetComponent<AbilityList>();
            Build build = abilityList.GetAbility(typeof(Build)) as Build;

            Entity entityToBuild = activeEntityToBuild;

            if (LeftClickMode == LeftClickMode.BuildStructure) {

                Blockable entityToBuildBlockable = entityToBuild.GetComponent<Blockable>();
                GeoRegion regionToBuildOn = entityToBuild.GetComponent<Blockable>().blockedRegion;

                entityToBuildBlockable.SetActive(false);

                // set gates
                if (entityToBuildBlockable.gaeanGates.Count > 0) {
                    geoLayers.humanGate.transform.parent = entityToBuild.transform;
                    geoLayers.humanGate.transform.position = entityToBuildBlockable.gaeanGates[0].CenterPoint.ToVector3() + new Vector3(0, 0.05f, 0);
                }

                //
                if (entityToBuildBlockable.machineGates.Count > 0) {
                    geoLayers.machineGate.transform.parent = entityToBuild.transform;
                    geoLayers.machineGate.transform.position = entityToBuildBlockable.machineGates[0].CenterPoint.ToVector3() + new Vector3(0, 0.05f, 0);
                }

                // check whether build region is on the map, if not, hide the build clone
                if (regionToBuildOn.OnMapCheck()) {

                    entityToBuild.SetDisplay(true);
                    build.SetBuildRegion(regionToBuildOn);

                    bool buildIsValid = build.Validate();

                    if (buildIsValid) {
                        entityToBuild.SetMaterial(Resources.Load("Green Transparent") as Material);
                    }
                    else {
                        entityToBuild.SetMaterial(Resources.Load("Red Transparent") as Material);
                    }

                }
                else {

                    entityToBuild.SetDisplay(false);

                }

            }
            else if (LeftClickMode == LeftClickMode.BuildPath) {

                Path pathModule = (Path)activeEntityToBuild.GetModule(typeof(Path));

                // building gate
                if (pathModule.IsGate) {

                    // set the build region
                    GeoRegion regionToBuildOn = entityToBuild.GetComponent<Blockable>().blockedRegion;
                    if (regionToBuildOn.OnMapCheck()) {

                        entityToBuild.SetDisplay(true);

                        if (build.SetBuildRegion(regionToBuildOn) && hasCost) {
                            entityToBuild.SetMaterial(Resources.Load("Green Transparent") as Material);
                        }
                        else {
                            entityToBuild.SetMaterial(Resources.Load("Red Transparent") as Material);
                        }

                    }

                    // determine gate rotation
                    GeoNode[] neighborNodes = geoMap.GetNeighborNodes(highlightedGeoNode);
                    for (int i = 0; i < neighborNodes.Length; i++) {
                        GeoNode neighborNode = neighborNodes[i];
                        if (neighborNode != null) {
                            if (neighborNode.blockableEntity != null) {

                                for (int j = 0; j < neighborNode.blockableEntity.StandardModule.Blockable.humanStops.Count; j++) {
                                    GeoNode humanStop = neighborNode.blockableEntity.StandardModule.Blockable.humanStops[j];
                                    if (humanStop.Equals(neighborNode)) {
                                        int rotation = 90 * i - 180;
                                        activeEntityToBuild.transform.rotation = Quaternion.Euler(new Vector3(0, rotation, 0));
                                    }
                                }

                                Path neighborPath = (Path)neighborNode.blockableEntity.GetModule(typeof(Path));
                                if (neighborPath != null) {
                                    int rotation = 90 * i - 180;
                                    activeEntityToBuild.transform.rotation = Quaternion.Euler(new Vector3(0, rotation, 0));
                                }
                            }
                        }
                    }

                    // building normal path
                }
                else {

                    // show new potential path
                    if (build.StartNode != null) {

                        bool pathNodesContainsHighlightedNode = false;
                        for (int i = 0; i < build.PathNodesBeingBuilt.Count - 1; i++) {
                            if (build.PathNodesBeingBuilt[i].Contains(highlightedGeoNode)) {
                                pathNodesContainsHighlightedNode = true;
                            }
                        }

                        bool highlightedNodeIsLastApprovedNode = false;
                        if (pathNodesContainsHighlightedNode) {
                            if (build.LastApprovedPathNode.Equals(highlightedGeoNode)) {
                                highlightedNodeIsLastApprovedNode = true;
                            }
                        }

                        if (!pathNodesContainsHighlightedNode) {

                            build.DisplayPotentialPathNodes();

                        }
                        else if (highlightedNodeIsLastApprovedNode) {

                            build.DisplayPotentialPathNodes(highlightedGeoNode);

                        }
                    }
                    else {


                        // set the build region
                        GeoRegion regionToBuildOn = entityToBuild.GetComponent<Blockable>().blockedRegion;
                        if (regionToBuildOn.OnMapCheck()) {

                            entityToBuild.SetDisplay(true);
                            build.SetBuildRegion(regionToBuildOn);

                        }

                        PathType pathType = UtilitiesManager.access.PathBuilder.GetPathType(highlightedGeoNode);
                        EntityType pathEntityType = (EntityType)Enum.Parse(typeof(EntityType), pathType.ToString());
                        //Destroy(stateManager.activeEntityToBuild);
                        DestroyActiveEntityToBuild();
                        SetActiveEntityToBuild(pathEntityType);
                        activeEntityToBuild.GetComponent<Blockable>().MoveBlockable(highlightedGeoNode.BotLeftPoint);

                        ValidateActiveBuild();
                        //stateManager.ValidateBuildingCost();

                        //geoLayers.RemoveStateEffects();
                        //PathType pathType = Util.PathBuilder.GetPathType(stateManager.highlightedGeoNode);
                        //GameObject pathObject = Instantiate(Resources.Load(pathType.ToString()) as GameObject);
                        //Effect pathEffect = new Effect(new List<GeoNode>() { stateManager.highlightedGeoNode }, new List<GameObject>() { pathObject });
                        //geoLayers.AddStateEffect(pathEffect);

                    }

                }



            }
        }
        */

        OnHighlightedMapNodeChange(node);

    }

    //
    /*
    public void ClickGlobalAbility(int index) {

        // set to ability state
        SetLeftClickMode(LeftClickMode.Ability);

        // set active ability and targets
        Ability ability = AbilityList.abilities[index];
        bool setAbility = false;
        if (ActiveAbility != null) {
            if (ActiveAbility.AbilityType != ability.AbilityType) {
                setAbility = true;
            }
        } else {
            setAbility = true;
        }

        if (setAbility) {

            ActiveAbility = ability;
            ActiveAbilityValidTargetTypes = ability.settings.Targets;
            ActiveAbilityTargets = new List<Target>();

            // set cursor
            Texture2D cursor = ability.settings.Cursor;
            Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
        } 
    }

    //
    public void AddAbilityTarget(Target target) {

        // validate target
        bool validTarget = false;
        bool isCorrectTargetType = target.type == ActiveAbilityValidTargetTypes[ActiveAbilityTargets.Count];
        if (isCorrectTargetType) {

            switch (target.type) {
                case TargetType.Gaean:
                    if (target.entity.entityClass == EntityClass.Human) {
                        validTarget = true;
                    }
                    break;
                case TargetType.Structure:
                    if (target.entity.entityClass == EntityClass.Structure || target.entity.entityClass == EntityClass.Path) {
                        validTarget = true;
                    }
                    break;
            }

        }

        // add target
        if (!validTarget || !ActiveAbility.ValidateTarget(ActiveAbilityTargets.Count, target)) { return; }
        ActiveAbilityTargets.Add(target);

        // if have enough targets, run ability
        bool isEnoughTargets = ActiveAbilityValidTargetTypes.Count == ActiveAbilityTargets.Count;
        if (isEnoughTargets) {
            ActiveAbility.Run(ActiveAbilityTargets);
            
            ActiveAbilityTargets = new List<Target>();
        }

    }
    */

    // 
    void UpdateRooms() {

        for (int i = 0; i < Rooms.Count; i++) {
            bool roomStillStands = Rooms[i].IsStillRoom();
            if (!roomStillStands) {
                Rooms.RemoveAt(i);
                i--;
            }
        }

        OnRoomsUpdated();

    }

    //
    public void AddRoom(Room room) {
        Rooms.Add(room);
        OnRoomAdded();
    }

    // highlighted geonode change event
    protected virtual void OnHighlightedMapNodeChange(TileOld node) {
        UpdateHighlightedMapNode?.Invoke(node);
    }

    //
    protected void OnGameSecondsTick() {
        GameSecondsTick?.Invoke(GameTimer);
    }

    //
    protected void OnStateSecondsTick() {
        StateSecondsTick?.Invoke(StateTimer);
    }

    //
    protected void OnStateChange() {
        StateChange?.Invoke(GameState);
    }

    //
    protected void OnUpdateEntityToBuild() {
        UpdateEntityToBuild(activeEntityTypeToBuild);
    }

    protected void OnRoomAdded() {
        RoomAdded?.Invoke(Rooms[Rooms.Count - 1]);
    }

    protected void OnRoomsUpdated() {
        RoomsUpdated?.Invoke(Rooms);
    }

    // 
    /*
    protected virtual void OnEntitySelected(WorldStateEventArgs args) {
        if (EntitySelected != null) {
            EntitySelected(this, args);
        }
    }
    */
}

public class Room {
    public List<TileOld> Tiles;
    public Room (List<TileOld> tiles) {
        Tiles = tiles;
    }

    public bool IsStillRoom() {

        bool isRoom = false;

        var ffa = new FloodFillAlgorithm();
        ffa.StartFillAlgorithm(Tiles[0],
            onFinish: (alg) => {
                if (!alg.outOfBounds) {
                    isRoom = true;
                }
                else {
                    isRoom = false;
                }
            }
        );

        return isRoom;

    }
}

