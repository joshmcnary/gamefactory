﻿/*
    Blockable determines its blocking nodes based off of its:
    OriginPoint - this is the center point of all the blocked nodes. this will be the center of a tile if size is odd
    Size - this is the width / height of the square for the blockable. not all nodes in the square have to be blocking, this is just the maximum

*/

using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

public class Blockable : MonoBehaviour {

    [Header("Set by User")]
    public int Size = 1;            // size of the square, used for determining the blocked nodes
    public List<bool> BlockingFlags;

    public Entity Entity { get; set; }

    // if blockable is active, it will add blocking to tiles. otherwise, it will only maintain its region and not block
    public bool Active { get { return Entity.EntityState == EntityState.Full; } }

    [Header("Set by Game")]
    public Point Origin;         // center point of the blockable, this will be the center of a node or an intersection of nodes depending on size
    public float Rotation;
    public List<Tile> Tiles;
    public bool IsOnValidPosition = false;    // this is false when on other blockables, set by ValidateMove

    //
    void OnValidate() {
        if (BlockingFlags.Count != Size * Size) {
            BlockingFlags = new List<bool>();
            for (int i = 0; i < Size * Size; i++) {
                BlockingFlags.Add(true);
            }
        }
    }

    //
    public void Run() {
        Entity = GetComponent<Entity>();
        Tiles = new List<Tile>();
    } 

    //
    private void SetNodes() {

        if (Size % 2 == 0) {
            Debug.LogWarning("Even sized blockables aren't yet implemented. Pick a different size!");
            return;
        }

        Tiles.Clear();

        Point startingPoint = new Point(Origin.x - Size / 2, Origin.z - Size / 2);
        Point endPoint = new Point(Origin.x + Size / 2, Origin.z + Size / 2);
        int count = 0;
        for (int z = (int)startingPoint.z; z <= endPoint.z; z++) {
            for (int x = (int)startingPoint.x; x <= endPoint.x; x++) {
                Tile node = GridData.access.GetTileNew(x, z);
                bool isBlockingNode = BlockingFlags[count];
                if (isBlockingNode) {
                    Tiles.Add(node);
                }
                count++;
            }
        }

        /*
        GeoNode originNode;
        if (size % 2 == 0) {
            Vector3 originNodeVector = Origin.ToVector3() - new Vector3(0.5f, 0, 0.5f) * (size - 1);
            originNode = GeoMap.access.GetGeoNode((int)originNodeVector.x, (int)originNodeVector.z);
        }
        else {
            Vector3 originNodeVector = Origin.ToVector3() - new Vector3(0.5f, 0, 0.5f) * (size - 1);
            originNode = GeoMap.access.GetGeoNode((int)originNodeVector.x, (int)originNodeVector.z);
        }

        blockedRegion = new GeoRegion(GeoRegionType.CornerSquare, originNode, size - 1);
        */
    }

    //
    public bool ValidateMove(Point origin, float rotation = 0) {

        Origin = origin;
        Rotation = rotation;
        SetNodes();

        IsOnValidPosition = false;

        // blockables test
        /*
        for (int i = 0; i < Tiles.Count; i++) {
            if (Tiles[i].blockableEntity != null) { return IsOnValidPosition; }
        }

        // water test
        Tile node = MapData.access.GetMapNode(origin.ToVector3());
        if (node.type == TileType.Water) { return IsOnValidPosition; }
        */

        //
        IsOnValidPosition = true;
        return IsOnValidPosition;

    }

    // originPoint
    // use this to determine the origin, then move the blockable
    public void MoveBlockable(Point origin, float rotationAngle = 0) {

        RemoveBlockedNodes();

        // set node0, the bottom left node of the blockable
        //Node0 = World.access.geoMap.GetGeoNode(new Vector3(entityPoint.x, entityPoint.y, entityPoint.z));
        //Debug.Log(new Vector3(entityPoint.x, entityPoint.y, entityPoint.z));

        // set origin
        //SetOrigin(origin);
        Origin = origin;
        RotateBlockable(rotationAngle);

        Point tempOriginFix = new Point(origin.ToVector3() + new Vector3(0.5f, 0, 0.5f));
        Entity.Move(tempOriginFix);

        // setup other blockable properties
        SetNodes();
        //SetKeyNodes();
        if (Entity.EntityState == EntityState.Full) {
            AddBlockableEntityToNodes();
        }

        if (Active) {
            //SetBlockingQuads();
        }
    }

    //
    void RotateBlockable(float rotationAngle) {

        // set the rotation property, should be either 0, 90, 180, or 360 for now
        Rotation += rotationAngle;
        if (Rotation < 0) {
            Rotation = 360 + Rotation;
        }
        Rotation %= 360;

        // rotate the entity
        //if (Origin != null) {
            
            transform.RotateAround(Origin.ToVector3(), Vector3.up, rotationAngle);
            
            // setup other blockable properties
            //SetKeyNodes();
        //}

        
        if (Active) {
            //SetBlockingQuads();
        }
    }

    // shortcut for rotate blockable, used to detect mousewheel movement
    public void RotateBlockable(int rotations) {
        MoveBlockable(Origin, rotations * 90f);
    }

    // sets origin based on node0position
    // origin should be the center pixel
    /*
    void SetOrigin(GeoPoint entityPoint) {
        Vector3 originOffset = size * new Vector3(0.5f, 0.02f, 0.5f);
        Origin = new GeoPoint(entityPoint.ToVector3() + originOffset);
    }
    */

    // this is because when blockable is not in game (ex: used as an entity effect), it does not add blockables
    // blockables may refer to nodes but not actually be blocking them
    public void AddBlockableEntityToNodes() {

        for (int i = 0; i < Tiles.Count; i++) {
            Tiles[i].AddBlockableEntity(Entity);
        }
    }

    //
    private void RemoveBlockedNodes() {
        if (Tiles.Count == 0)
            return;

        foreach(Tile tile in Tiles) {
            tile.RemoveBlockableEntity();
        }

        Tiles.Clear();
    }

    //
    public void RemoveFromMap() {
        RemoveBlockedNodes();
    }
}
