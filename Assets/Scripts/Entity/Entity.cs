﻿/*
    Entity allows for easier control of standard unity functionality and attached components/modules

    Entity is the only component that is considered fully active for the lifetime of an entity
    Other components can have different functionality depending on entity state
*/

using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;

public class AnimationEndEventArgs : EventArgs {
    public int StateHash { get; set; }
}
public delegate void AnimationEndEventHandler(Entity sender, AnimationEndEventArgs args);

public class Entity : NetworkBehaviour, IPointerDownHandler, IPointerEnterHandler {

    public event System.Action RemovedFromGame;

    static int EntityLayer = 10;

    // settings are the basis of how an entity is defined. without settings an entity is nothing!
    // baseSettings are set in the entity's prefab
    // settings are an instantiation of this
    /*
    public EntitySettings defaultSettings;
    public EntitySettings settings { get; private set; }
    */

    // enums should be used to store type rather than string
    //public EntityClass entityClass { get; private set; }
    public EntityType entityType;

    // required unity components
    public Rigidbody Rbody;
    public Animator Animator;
    private MeshCollider[] meshColliders;
    private MeshRenderer[] meshRenderers;
    SkinnedMeshRenderer[] SkinnedMeshRenderers;
    private Material[] StoredMaterials;

    // game components
    public Blockable Blockable;

    // modules
    // @TODO should set up a smart way to cache all components and make them easy to reference
    /*
    public ModuleSettings[] moduleSettings { get; private set; }
    public Module[] modules { get; private set; }
    public StandardModule StandardModule { get { if (modules != null) { return modules.Length > 0 ? (StandardModule)modules[0] : null; } return null; } }
    */

    //
    public EntityState EntityState;

    // this is equal to NetworkIdentity.netId -- which is a uint but should not create enough entities for this to matter, ie: >2147483647
    [SyncVar(hook=nameof(OnSyncId))]
    public int id = 0;

    // if entity is not active, it will have all script components disabled except entity
    public bool Active { get; private set; }

    // if entity is displaying, mesh renderers will be active
    private bool display = false;

    // used to control functionality based on whether entity is considered part of the game
    public bool InGame = false;

    // this can probably be removed and replaced by EntityState.Destroyed
    public bool BeingDestroyed = false;

    // animation related
    /*
    public Transform rootBone;
    public Vector3 rootPosition;
    public bool stateEnded;
    public int stateHash;
    */
    public event AnimationEndEventHandler AnimationEnd;
    public Dictionary<int, string> animationStateHashes { get; private set; }

    // for fading
    private enum FadeState { Opaque, Transparent, FadingIn, FadingOut }
    private FadeState _FadeState;
    private float FadeDuration;
    private float FadeTimer;

    // holds whether the entity is on or off the map
    // is controlled by addtomap and removefrommap
    private bool OnMap = true;

    // holds the point that entity is on, which will be the transform position
    // the point of an entity should be its bottom-center position
    // for example: a 2x2 structure built at the bottom left corner will have a point of (1, 0, 1) -- assuming height/y is 0
    public Point point { get { return new Point(transform.position); } }

    // quick reference to transform position
    public Vector3 Position { get { return transform.position; } }

    // events
    public event EventHandler Selected;
    public event EventHandler RightPress;

    // THIS SHOULD BE REMOVED, ENTITY SHOULD NOT NEED TO KNOW WHAT NODE IT IS ON, THIS SHOULD BE DETERMINED BY MOVABLE/BLOCKABLE -- ONLY NEEDS TO KNOW GEOPOINT
    // get the node the entity is on, currently based off of where transform position is
    public Tile node { get { return World.access.GridData.GetTileNew((int)gameObject.transform.position.x, (int)gameObject.transform.position.z); } }  
    
    // used for collisions, where needed
    public GameObject Hitbox { get; set; }

    //
    public void OnSyncId(int newId) {
        if (isClientOnly && newId != -1) {
            EntityManager.access.entitiesNew.Add(newId, this);
        }
    }

    // catch all for any updates that need to happen on an entity outside of actionqueue
    // TODO: this should be made the main update function, cascades to another function of which name im unsure
    void Update() {

        if (Active || EntityState == EntityState.Basic) {

            /*
            if (StandardModule != null) {
                Blockable blockable = StandardModule.Blockable;
                if (blockable != null) {
                    //blockable.Run();
                }

            }
            */

        }

        // update queue
        if (_FadeState == FadeState.FadingIn || _FadeState == FadeState.FadingOut) {
            //FadeUpdate();
        }


    }

    // when entities change states, various components will be turned on/off as functionality becomes relevant/problematic
    // Default -- when the default entities are initially loaded, this will perform the needed functions to make sure default entities are stable
    public void SetState(EntityState entityState) {

        switch (entityState) {

            case EntityState.Default:                

                // 
                //InstantiateSettings();                   

                break;

            case EntityState.Basic:

                if (EntityState == EntityState.Default) {

                    // instantiate a copy of defaultsettings
                    // InstantiateSettings();

                    // add the 3d model / meshes
                    // InitModel();

                    // add all other required unity components
                    // AddRequiredComponents();

                    // add game-related modules
                    // AddModules();

                    //SetActive(false);
                    //SetDisplay(true);

                    Blockable = GetComponent<Blockable>();
                    if (Blockable != null) {
                        Blockable.Run();
                    }

                }
                else if (EntityState == EntityState.Full) {
                    //SetActive(false);
                }

                break;

            case EntityState.Full:
                
                //SetActive(true);

                break;
        }

        EntityState = entityState;
    }

    // instantiates a copy of default settings 
    /*
    private void InstantiateSettings() {

        //
        if (defaultSettings != null) {

            settings = Instantiate(defaultSettings);

            name = settings.name;
            entityClass = settings.entityClass;
            entityType = settings.entityType;

            gameObject.layer = 10;

        } else {
            Debug.Log(name + " doesn't have default settings");

            // default entity settings if there aren't any
            modules = new Module[0];
        }

        //
        animationStateHashes = new Dictionary<int, string>();

        //
        if (settings.HitboxEnabled) {

            Hitbox = GameObject.CreatePrimitive(settings.HitBoxShape);
            Hitbox.name = "Hitbox";
            Hitbox.transform.parent = gameObject.transform;
            Hitbox.transform.position = Position + settings.HitBoxOffset;
            Hitbox.transform.localScale = Vector3.Scale(Hitbox.transform.localScale, settings.HitboxScale);
            Hitbox.layer = EntityLayer;
            Hitbox.AddComponent<Rigidbody>().isKinematic = true;
            //Hitbox.GetComponent<CapsuleCollider>().enabled = false;
            if (!settings.HitboxDisplayed) {
                Hitbox.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
    */

    // performs any actions that need to be done to the base instantiated model to make it usable as an entity
    private bool InitModel() {

        /*
        // delete hipoly meshes
        Transform hiPolyTransform = transform.Find("HiPoly");
        if (hiPolyTransform != null) {
            GameObject hiPolyObjects = hiPolyTransform.gameObject as GameObject;
            Destroy(hiPolyObjects);
        }
        
        // add colliders to all mesh objects
        for (int i = 0; i < transform.childCount; i++) {
            GameObject tempObject = transform.GetChild(i).gameObject;
            //tempObject.layer = 10;
            MeshRenderer meshRenderer = tempObject.GetComponent<MeshRenderer>();
            SkinnedMeshRenderer skinnedMeshRenderer = tempObject.GetComponent<SkinnedMeshRenderer>();
            if (meshRenderer != null) {                

                MeshCollider collider = tempObject.AddComponent<MeshCollider>();
                if (collider.sharedMesh.vertices.Length < 256) {
                    collider.convex = true;
                } else {
                    // Debug.Log(collider.gameObject.name + " has too many vertices to create a convex mesh in MeshCollider");
                }

            } else if (skinnedMeshRenderer != null) {                

                Mesh mesh = new Mesh();
                skinnedMeshRenderer.BakeMesh(mesh);

                MeshCollider collider = tempObject.AddComponent<MeshCollider>();

                if (mesh.vertices.Length < 256) {
                    collider.convex = true;
                }
                else {
                    // Debug.Log(collider.gameObject.name + " has too many vertices to create a convex mesh in MeshCollider");
                }
                //collider.convex = true;
                collider.sharedMesh = mesh;

                tempObject.transform.localScale = Vector3.one;
            }

            //tempObject.AddComponent<Rigidbody>().isKinematic = true;
        }        

        // setup Animator
        Animator = GetComponent<Animator>();
        if (Animator != null) {
            RuntimeAnimatorController animatorController = (RuntimeAnimatorController)Resources.Load("AnimatorControllers/" + entityType);
            Animator.runtimeAnimatorController = animatorController;
        }

        // cache colliders and renderers
        meshColliders = GetComponentsInChildren<MeshCollider>();
        meshRenderers = GetComponentsInChildren<MeshRenderer>();
        SkinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();


        */
        return true;
        

    }

    //
    private void AddRequiredComponents() {

        /*
        if (settings.rigidBody) {
            // Rbody = (Rigidbody)AddComponent(typeof(Rigidbody));
            // Rbody.isKinematic = true;
        }
        */
    }

    // loads the modules from module settings and adds appropriate components
    /*
    private void AddModules() {

        // setup module array
        moduleSettings = settings.moduleSettings;
        modules = new Module[moduleSettings.Length + 1];

        // load standard module
        if (settings.standardSettings != null) {
            StandardModuleSettings standardModuleSettings = Instantiate(settings.standardSettings);
            StandardModule standardModule = (StandardModule)AddModule(typeof(StandardModule));
            modules[0] = standardModule;
            standardModule.LoadSettings(standardModuleSettings);
            standardModule.InitSettings(standardModuleSettings);
        } else {
            Debug.Log(name + " doesn't have standard module settings");
        }

        // load extra modules
        for (int i = 1; i < moduleSettings.Length + 1; i++) {

            ModuleSettings _settings = moduleSettings[i - 1];
            Module module;

            // if there are module specific settings, use the type set by the init method
            // otherwise use the name field, which needs to be set in ModuleSettingsMenuItems
            if (_settings.type != null) {
                module = AddModule(_settings.type);
            } else {
                module = AddModule(Type.GetType(_settings.name));
            }
            
            // initialize module and add to modules list
            module.settings = _settings;
            module.InitSettings(_settings);
            modules[i] = module;
        }

    }

    // this is a wrapper method for the underlying unity functionality to add components to gameobjects
    private Component AddComponent(Type type) {
        return gameObject.AddComponent(type);
    }

    //
    public Module AddModule(Type type) {
        return (Module)AddComponent(type);
    }

    public Module GetModule(Type type) {

        Module module = null;

        for (int i = 1; i < modules.Length; i++) {
            if (modules[i].GetType() == type) {
                module = modules[i];
                i = modules.Length;
            }
        }

        return module;

    }
    */

    //
    public void AddToMap(Point origin, float rotation = 0) {

        Blockable.MoveBlockable(origin, rotation);

        OnMap = true;

        // verify that entity is in entityonly state since this method will change state to allmodules
        /*
        if (EntityState != EntityState.EntityOnly) {
            Debug.Log("Entity should be in EntityOnly state to be added to map");
            return;
        }
        */

        // change state
        //SetState(EntityState.Full);

        // specific-module handling when added to map
        /*
        GeoPoint addPoint;
        if (entityClass == EntityClass.Structure || entityClass == EntityClass.Path) {

            addPoint = node.BotLeftPoint;
            
            if (offset != null) {
                addPoint = addPoint.AddPoint(offset);
            }

            StandardModule.Blockable.MoveBlockable(addPoint, rotation);

        } else if (entityClass == EntityClass.Machine || entityClass == EntityClass.Human) {
            
            addPoint = node.CenterPoint;

            if (offset != null) {
                addPoint = addPoint.AddPoint(offset);
            }

            // make sure the position the entity is on is on a navmesh
            NavMeshHit closestHit;
            if (NavMesh.SamplePosition(addPoint.ToVector3(), out closestHit, 500, 1)) {
                Move(new GeoPoint(closestHit.position));
            }

            // NavMeshAgent should be added when entity is added to map, otherwise Unity will produce an error because the entity will not be on the NavMeshSurface
            // until it is moved onto to the map
            NavMeshAgent agent = gameObject.AddComponent<NavMeshAgent>();
            StandardModule.Movable.SetAgent(agent);

            if (entityClass == EntityClass.Machine) {
                GetComponent<Machine>().AddedToMap();
            }
        }

        //
        for (int i = 0; i < modules.Length; i++) {
            modules[i].AddedToMap();            
        }

        //
        for (int i = 0; i < modules.Length; i++) {
            modules[i].AfterAddedToMap();
        }
        */
    }

    //
    public void RemoveFromMap() {

        OnMap = false;
        Move(GridData.offMapPoint);
        GetComponent<Blockable>().RemoveFromMap();
    }

    // turns on/off all script components except entity script
    // should mainly apply to update scripts, which should be constrained to entity and actionqueue
    public void SetActive(bool activeFlag) {
         
        if (meshColliders != null) {
            foreach (MeshCollider collider in meshColliders) {
                collider.enabled = activeFlag;
            }
        } 

        /*
        if (StandardModule != null) {
            StandardModule.SetActive(activeFlag);
        } 

        foreach (Module module in modules) {
            module.enabled = activeFlag;
        }
        */

        SetDisplay(activeFlag);

        //hitbox
        if (Hitbox != null) {
            Hitbox.SetActive(activeFlag);
        }

        Active = activeFlag;

    }

    // changes material of all meshes
    public void SetMaterial(Material material) {
        
        if (meshRenderers == null) {
            meshRenderers = GetComponentsInChildren<MeshRenderer>();
            StoredMaterials = new Material[meshRenderers.Length];
            for (int i = 0; i < StoredMaterials.Length; i++) {
                StoredMaterials[i] = meshRenderers[i].material;
            }
        }

        foreach (MeshRenderer renderer in meshRenderers) {

            Material[] materials = renderer.materials;
            for (int i = 0; i < materials.Length; i++) {
                materials[i] = material;
            }
            renderer.materials = materials;

        }

    }

    //
    public void ResetMaterials() {

        if (StoredMaterials != null) {
            for (int i = 0; i < meshRenderers.Length; i++) {
                meshRenderers[i].material = StoredMaterials[i];
            }
        }

    }

    // displays/hides the entity
    public void SetDisplay(bool showEntity) {        

        if (showEntity && !display) {

            foreach (MeshRenderer renderer in meshRenderers) {

                if (renderer.name != "Hitbox") {
                    renderer.enabled = true;
                }

            }
            display = true;

        } else if (!showEntity && display) {

            foreach (MeshRenderer renderer in meshRenderers) {
                if (renderer == null) { continue; }
                renderer.enabled = false;
            }
            display = false;

        }

    }

    //
    public void SetColliders(bool enabled) {

        for (int i = 0; i < meshColliders.Length; i++) {

            MeshCollider collider = meshColliders[i];
            collider.enabled = enabled;

        }

    }

    //
    public void Play(string animationStateName, int layer = 0) {
        
        if (Animator == null) { Debug.Log("There is no animator attached to " + name); return; }

        int stateHash = Animator.StringToHash(animationStateName);

        if (!Animator.HasState(layer, stateHash)) { Debug.Log(entityType + " is trying to play animation state " + animationStateName + " but does not have it."); return; }        

        if (!animationStateHashes.ContainsKey(stateHash)) {
            animationStateHashes.Add(stateHash, animationStateName);
        }

        Animator.Play(animationStateName, layer);

    }

    // multiplier should be between 0 and 1
    public void SetTransparency(float multiplier) {

        foreach (MeshRenderer renderer in meshRenderers) {
            if (renderer == null) { continue; }
            Material material = renderer.material;
            Color newColor = material.color;
            newColor.a = multiplier;
            material.color = newColor;
        }

        foreach (SkinnedMeshRenderer renderer in SkinnedMeshRenderers) {
            if (renderer == null) { continue; }
            Material material = renderer.material;
            Color newColor = material.color;
            newColor.a = multiplier;
            material.color = newColor;
        }

    }

    //
    /*
    public void StartFade(bool fadeIn, float seconds) {

        // needs to assume ActionQueue is active and not already doing a fade
        if (_FadeState == FadeState.FadingIn || _FadeState == FadeState.FadingOut) { return; }

        FadeDuration = seconds;
        FadeTimer = 0;

        if (fadeIn && _FadeState == FadeState.Transparent) {
            
            SetDisplay(true);
            _FadeState = FadeState.FadingIn;

        }
        else if (!fadeIn && _FadeState == FadeState.Opaque) {
            
            _FadeState = FadeState.FadingOut;

            // change material to transparent mode
            foreach (MeshRenderer renderer in meshRenderers) {
                if (renderer == null) { continue; }
                Utilities.ChangeRenderMode(renderer.material, Utilities.BlendMode.Transparent);
            }

            foreach (SkinnedMeshRenderer renderer in SkinnedMeshRenderers) {
                if (renderer == null) { continue; }
                Utilities.ChangeRenderMode(renderer.material, Utilities.BlendMode.Transparent);
            }

        }

    }

    //
    public void FadeUpdate() {

        FadeTimer += Time.deltaTime;
        if (FadeTimer < FadeDuration) {     // fade still happening

            if (_FadeState == FadeState.FadingIn) {
                SetTransparency(FadeTimer / FadeDuration);
            }
            else if (_FadeState == FadeState.FadingOut) {
                SetTransparency(1 - (FadeTimer / FadeDuration));
            }

        }
        else {      // fade over            

            if (_FadeState == FadeState.FadingIn) {

                _FadeState = FadeState.Opaque;

                // change material to opaque mode
                foreach (MeshRenderer renderer in meshRenderers) {
                    Utilities.ChangeRenderMode(renderer.material, Utilities.BlendMode.Opaque);
                }

            }
            else if (_FadeState == FadeState.FadingOut) {

                SetDisplay(false);
                _FadeState = FadeState.Transparent;

            }
        }

    }

    //
    public void StartFadeOld(bool fadeIn, float seconds) {

        // needs to assume ActionQueue is active and not already doing a fade
        if (EntityState != EntityState.AllModules && (_FadeState == FadeState.FadingIn || _FadeState == FadeState.FadingOut)) { return; }

        ActionQueue queue = StandardModule.ActionQueue;

        if (fadeIn && _FadeState == FadeState.Transparent) {

            ActionGroup fadeInActionGroup = new ActionGroup();
            Action fadeInAction = new Action();
            fadeInAction.Trigger += delegate {
                SetDisplay(true);
                _FadeState = FadeState.FadingIn;
                FadeDuration = seconds;
                FadeTimer = 0;
            };
            fadeInAction.Fire += delegate {
                FadeUpdateOld();
            };
            fadeInActionGroup.AddUpdateAction(fadeInAction);
            StandardModule.ActionQueue.AddActionsToEnd(fadeInActionGroup);

        } else if (!fadeIn && _FadeState == FadeState.Opaque) {

            ActionGroup fadeOutActionGroup = new ActionGroup();
            Action fadeOutAction = new Action();
            fadeOutAction.Trigger += delegate {
                _FadeState = FadeState.FadingOut;
                FadeDuration = seconds;
                FadeTimer = 0;
            };
            fadeOutAction.Fire += delegate {
                FadeUpdateOld();
            };
            fadeOutActionGroup.AddUpdateAction(fadeOutAction);
            StandardModule.ActionQueue.AddActionsToEnd(fadeOutActionGroup);

            // change material to transparent mode
            foreach (MeshRenderer renderer in meshRenderers) {
                Utilities.ChangeRenderMode(renderer.material, Utilities.BlendMode.Transparent);
            }

        }

    }

    //
    public void FadingDeath(float seconds) {
        StartFade(false, seconds);
        StartCoroutine(DestroyTimer(seconds));
    }
    */

    //
    IEnumerator DestroyTimer(float waitSeconds) {

        yield return new WaitForSeconds(waitSeconds);

        //EntityManager.access.DestroyEntity(this);
    }

    //
    /*
    public void FadeUpdateOld() {
        
        FadeTimer += Time.deltaTime;
        if (FadeTimer < FadeDuration) {

            if (_FadeState == FadeState.FadingIn) {
                SetTransparency(FadeTimer / FadeDuration);
            } else if (_FadeState == FadeState.FadingOut) {
                SetTransparency(1 - (FadeTimer / FadeDuration));
            }

        } else {

            if (_FadeState == FadeState.FadingIn) {

                _FadeState = FadeState.Opaque;

                // change material to opaque mode
                foreach (MeshRenderer renderer in meshRenderers) {
                    Utilities.ChangeRenderMode(renderer.material, Utilities.BlendMode.Opaque);
                }

            } else if (_FadeState == FadeState.FadingOut) {

                SetDisplay(false);
                _FadeState = FadeState.Transparent;

            }

            // remove from ActionQueue
            StandardModule.ActionQueue.CompleteAction();
        }

    }
    */

    // all entity movement should be performed here
    public void Move(Point _point) {
        //point = _point;
        transform.position = new Vector3(_point.x, _point.y, _point.z);
    }

    // used to have an entity exit the map
    /*
    public void ExitMap(GeoPoint exitPoint) {

        StartFade(false, 2);

        //
        onMap = false;

        // begin transition process moving from current point to exit point

    }

    // used to have an entity exit the map
    public void EnterMap(GeoPoint enterPoint, GeoPoint startingPoint = null) {

        // if starting point is null, use entitys current point as starting point
        // if starting point is set, move entity to starting point
        // begin transition process

        //
        onMap = true;

    }

    //
    public void RemoveFromGameAndExitMap(GeoNode exitNode) {

        EntityManager.access.RemoveEntityFromGame(this);

        MoveRoute exitRoute = new MoveRoute();
        ActionGroup fadeActions = new ActionGroup();
        fadeActions.AddSyncAction(delegate {
            ExitMap(exitNode.CenterPoint);
        });
        fadeActions.AddWaitAction(2);
        fadeActions.AddSyncAction(delegate {
            EntityManager.access.DestroyEntity(this); });
        exitRoute.AddRouteSegment(exitNode, null, fadeActions);
        StandardModule.Movable.SetCurrentRoute(exitRoute);

    }

    //
    public void Explode(Vector3 explodePosition, float explosiveForce, float explosiveRadius, float upwardForce) {

        if (entityClass != EntityClass.Human) { return; }

        // entity specific
        Play("Idle");
        SetActive(false);
        SetDisplay(true);

        if (StandardModule.Movable != null) {
            StandardModule.Movable.Agent.enabled = false;
        }

        //Time.timeScale = 0.1f;

        //
        Transform rootTransform = transform.Find("Armature").GetChild(0);
        for (int i = 0; i < meshColliders.Length; i++) {
            MeshCollider collider = meshColliders[i];
            GameObject go = collider.gameObject;
            Rigidbody rbody = null;            

            if (collider.sharedMesh.name == "") {
                collider.sharedMesh = go.GetComponent<SkinnedMeshRenderer>().sharedMesh;
                Transform boneTransform = rootTransform.Find(go.name + "_0");
                if (boneTransform != null) {
                    rbody = boneTransform.gameObject.GetComponent<Rigidbody>();
                    if (rbody != null) { return; }      // this means a rigidbody has already been added due to previous explosion
                    rbody = boneTransform.gameObject.AddComponent<Rigidbody>();
                } else {
                    continue;
                }
            } else {
                rbody = go.GetComponent<Rigidbody>();
                if (rbody != null) { return; }      // this means a rigidbody has already been added due to previous explosion
                rbody = go.AddComponent<Rigidbody>();
            }

            rbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            rbody.isKinematic = false;
            collider.convex = true;
            collider.enabled = true;

            rbody.AddExplosionForce(explosiveForce, explodePosition, explosiveRadius, upwardForce);
        }

        Instantiate(Resources.Load("Helpers/BloodMist") as GameObject, Position, Quaternion.identity);
        StartFade(false, 5);
    }

    //
    public void Save() {
        //serializedSavePosition = pos;
    }

    //This can be used by inherriting classes to make data classes save
    public virtual void Load() {
        // go = EntitySystem.LoadGameObject(prefabName, serializedSavePosition);
    }
    */

    //
    void MousePress(bool leftMouse) {

        if (leftMouse) {
            OnSelected(EventArgs.Empty);
        }
        else {
            OnRightPress();
        }

    }

    // Monobehaviour mousedown method
    // preferred behavior is probably IPointerClickHandler as seen in surface
    /*
    public void OnMouseDown() {
        MousePress(true);
    }
    */

    public void OnSelected(EventArgs e) {
        if (Selected != null)
            Selected(this, e);
    }

    //
    public void OnRightPress() {
        if (RightPress != null)
            RightPress(this, EventArgs.Empty);
    }

    // method for IPointerDownHandler
    public void OnPointerDown(PointerEventData pointerData) {
        if (Input.GetMouseButtonDown(0)) {
            MousePress(true);
        }
        else if (Input.GetMouseButtonDown(1)) {
            MousePress(false);
        }
    }

    //
    public void OnPointerEnter(PointerEventData pointerData) {

        World.access.State.SetHighlightMode(HighlightMode.Entity);

        /*
        if (WorldStateManager.access.LeftClickMode == LeftClickMode.BuildStructure) {
            World.access.stateManager.SetHighlightMode(HighlightMode.GeoView);
        } else {
            World.access.stateManager.SetHighlightMode(HighlightMode.Entity);
        }
        */

    }

    //
    public void OnAnimationEnd(int stateHash) {
        if (AnimationEnd != null) {
            AnimationEnd(this, new AnimationEndEventArgs() { StateHash = stateHash });
        }
    }

    public EntityData ToData() {
        return new EntityData(this);
    }

    //
    public void AddToGame() {

        SetState(EntityState.Full);

        // OnAddedToGame();
    }

    public void RemoveFromGame() {

        SetState(EntityState.Basic);
        //BeingDestroyed = true;
        //StandardModule.ActionQueue.ClearAllActions(true);
        OnRemovedFromGame();

    }

    public void Destroy() {

        BeingDestroyed = true;

        /*
        for (int i = 0; i < modules.Length; i++) {
            modules[i].Destroy();
        }
        */

    }

    //
    protected void OnRemovedFromGame() {
        if (RemovedFromGame != null) {
            RemovedFromGame();
        }
    }
}

//
[Serializable]
public class EntityData {
    
    public EntityType entityType;
    public Point point;

    public EntityData(Entity entity) {
        entityType = entity.entityType;
        point = entity.point;
    }

}
