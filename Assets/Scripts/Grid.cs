﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public LayerMask gridPieceLayer;
    public GameObject gearPiecePrefab;

    public Tile lastHoveredTile = new Tile(-1, -1, null);
    public Color normalColor, highlightedColor;

    public Dictionary<Point, Tile> tileDict = new Dictionary<Point, Tile>();

    Vector2 tileSize = new Vector2(5, 5);
    int tileSpacing = 0;
    int rows = 10;
    int cols = 10;
    int offsetX = 0;
    int offsetY = 0;

    void Awake()
    {
        CreateGrid();
    }

    void FixedUpdate()
    {
        MouseOverTile();
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            var tile = GetApproximateTile(MouseToWorldPosition());
            bool isBlocked = tile.Type == TileType.blocked;
            tile.Type = TileType.blocked;
            if (!isBlocked)
            {
                InitializeFloodFillAlgorithm(tile);
            }
        }
        if (Input.GetMouseButton(1))
        {
            var tile = GetApproximateTile(MouseToWorldPosition());
            bool isOpen = tile.Type == TileType.open;
            tile.Type = TileType.open;
            if (!isOpen)
            {
                InitializeFloodFillAlgorithm(tile, false);
            }
        }
    }

    [ContextMenu("*** Create Grid GameObjects")]
    public void CreateGrid()
    {


        List<GameObject> gs = new List<GameObject>();
        foreach (Transform t in gameObject.transform)
        {
            gs.Add(t.gameObject);
        }

        for (int i = gs.Count - 1; i > -1; i--)
        {
            if (!Application.isPlaying)
            {
                GameObject.DestroyImmediate(gs[i]);
            }
            else
            {
                GameObject.Destroy(gs[i]);
            }
        }

        tileDict.Clear();

        int x = 0;
        int y = 0;
        for (int i = 0; i < (cols + 1) * (rows + 1); i++)
        {
            Vector3 pos = new Vector3(offsetX + x * tileSize.x + x * tileSpacing, offsetY + y * tileSize.y + y * tileSpacing);
            var go = Instantiate(gearPiecePrefab, pos, Quaternion.identity, transform) as GameObject;
            go.transform.localScale = tileSize;
            go.transform.SetParent(transform);

            var t = new Tile(x, y, go);
            var p = t.point;
            if (!tileDict.ContainsKey(p))
            {
                tileDict.Add(p, t);
            }
            else
            {
                Debug.LogError("error in creating grid... KEY ALREADY EXISTS!");
            }

            x++;
            if (x > cols)
            {
                x = 0;
                y++;
            }
        }
    }

    Vector3 TileMapStartingLocation()
    {
        return new Vector3(offsetX, offsetY);
    }

    public Tile GetApproximateTile(Vector3 pos)
    {
        //do some calculation to determine closest tile to world position then put the correct point in realx/
        pos -= TileMapStartingLocation();

        pos.x += tileSize.x * 0.5f;
        pos.y += tileSize.y * 0.5f;

        int realX = Mathf.FloorToInt(pos.x / (tileSize.x + tileSpacing));
        int realY = Mathf.FloorToInt(pos.y / (tileSize.y + tileSpacing));

        return GetTile(realX, realY);
    }

    public Tile GetTile(int x, int y)
    {
        var p = new Point() { x = x, y = y };
        if (tileDict.ContainsKey(p))
        {
            return tileDict[p];
        }
        else
        {
            return new Tile(-1, -1, null);
        }
    }

    Vector3 MouseToWorldPosition()
    {
        var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0;
        return pos;
    }

    private void MouseOverTile()
    {


        lastHoveredTile.DeselectTile();
        var tile = GetApproximateTile(MouseToWorldPosition());
        tile.SelectTile();
        lastHoveredTile = tile;

        //physics raycast to hit tile
        //var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero, 1, gridPieceLayer);
        //Debug.DrawRay(Camera.main.ScreenToWorldPoint(pos), Vector2.right, Color.red, 1f);
        //if (hit)
        //{
        //    if (lastHoveredTile != null && hit.collider.gameObject != lastHoveredTile)
        //    {
        //        lastHoveredTile.GetComponent<SpriteRenderer>().color = normalColor;
        //    }

        //    lastHoveredTile = hit.collider.gameObject;

        //    var sr = lastHoveredTile.GetComponent<SpriteRenderer>();
        //    sr.color = highlightedColor;
        //}
        //else
        //{
        //    if (lastHoveredTile != null)
        //    {
        //        lastHoveredTile.GetComponent<SpriteRenderer>().color = normalColor;
        //    }
        //}
    }

    private void InitializeFloodFillAlgorithm(Tile initialTile, bool useAdjacentTiles = true)
    {
        var adjacentToInitial = new List<Tile>();
        if (useAdjacentTiles)
        {
            int numOfWallsNear = 0;
            adjacentToInitial = AdjacentTiles(initialTile, true, (checkTile) =>
            {
                if (checkTile.Type == TileType.blocked)
                {
                    numOfWallsNear++;
                }
                return checkTile.tileObject != null && checkTile.Type == TileType.open;
            });

            if (numOfWallsNear < 2 && adjacentToInitial.Count > 0)
            {
                adjacentToInitial = new List<Tile>() { adjacentToInitial[0] };
            }

        }
        else
        {
            adjacentToInitial.Add(initialTile);
        }

        //run the algorithm for all 4 tiles around the initial tile.
        foreach (var t in adjacentToInitial)
        {
            var ffa = new FloodFillAlgorithm();
            ffa.StartFillAlgorithm(t, this,
                onFinish: (alg) =>
                {
                    if (!alg.outOfBounds)
                    {
                        foreach (var a in alg.openList)
                        {
                            a.SetPlayerColor(Color.cyan);
                        }
                    }
                    else
                    {
                        foreach (var a in alg.openList)
                        {
                            a.SetPlayerColor(Color.white);
                        }
                    }
                }
            );

            //if (!ffa.outOfBounds)
            //{
            //    foreach (var o in ffa.openList)
            //    {
            //        o.SetPlayerColor(Color.cyan);
            //    }
            //}
        }
    }

    private List<Tile> AdjacentTiles(Tile tile, bool includeCorners, Func<Tile, bool> checkCondition = null)
    {
        List<Tile> returnListTiles = new List<Tile>();
        List<Point> points = new List<Point>()
        {
            new Point(tile.point.x - 1, tile.point.y),
            new Point(tile.point.x + 1, tile.point.y),
            new Point(tile.point.x, tile.point.y + 1),
            new Point(tile.point.x, tile.point.y - 1),
        };
        if (includeCorners)
        {
            points.Add(new Point(tile.point.x - 1, tile.point.y - 1));
            points.Add(new Point(tile.point.x + 1, tile.point.y + 1));
            points.Add(new Point(tile.point.x + 1, tile.point.y - 1));
            points.Add(new Point(tile.point.x - 1, tile.point.y + 1));
        }

        foreach (var p in points)
        {
            var checkTile = GetTile(p.x, p.y);
            if (checkCondition == null || checkCondition(checkTile))
            {
                returnListTiles.Add(checkTile);
            }
        }

        return returnListTiles;
    }

    public enum TileType { open, blocked }

    public class Tile
    {
        public TileType Type
        {
            get { return type; }
            set
            {
                type = value;
                SetColor();
            }
        }
        private TileType type = TileType.open;

        public GameObject tileObject;
        private SpriteRenderer tileSR;
        public Point point;
        private Color color;
        private Color playerColor = Color.white;

        bool tileSelected = false;

        public void SetPlayerColor(Color color)
        {
            playerColor = color;
            SetColor();
        }

        public void SelectTile()
        {
            tileSelected = true;
            color = Color.red;
            SetColor();
        }

        public void DeselectTile()
        {
            tileSelected = false;
            SetColor();
        }

        public Tile(int x, int y, GameObject go)
        {
            this.point = new Point() { x = x, y = y };
            this.tileObject = go;

            if (this.tileObject != null)
            {
                this.tileSR = go.transform.GetComponent<SpriteRenderer>();
            }

            this.type = TileType.open;
        }

        private void SetColor()
        {
            if (!tileSelected)
            {
                switch (type)
                {
                    case TileType.open:
                        if (playerColor != Color.white)
                        {
                            color = playerColor;
                        }
                        else
                        {
                            color = Color.white;
                        }

                        break;

                    case TileType.blocked:
                        color = Color.blue;
                        break;
                }
            }
            if (tileSR != null)
                tileSR.color = color;
        }

    }

    [System.Serializable]
    public struct Point
    {
        public int x, y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public class FloodFillAlgorithm
    {
        public Grid grid;
        public bool outOfBounds = false;
        public HashSet<Tile> searchHash = new HashSet<Tile>();
        public HashSet<Tile> openList = new HashSet<Tile>();
        public HashSet<Tile> blockedList = new HashSet<Tile>();

        public Action<FloodFillAlgorithm> finishedCallback = null;

        int searchCount = 0;
        int searchLimit = 500;

        public void StartFillAlgorithm(Tile initialTile, Grid grid, Action<FloodFillAlgorithm> onFinish)
        {
            finishedCallback = onFinish;
            this.grid = grid;
            searchHash.Add(initialTile);
            SearchForMore();
        }

        public void SearchForMore()
        {
            searchCount++;
            if (searchCount > searchLimit)
            {
                Debug.LogError("over search limit");
                FinishedAlgorithm();
                return;
            }
            if (searchHash.Count > 0)
            {
                var tile = searchHash.First();
                if (tile.tileObject == null)
                {
                    outOfBounds = true;
                }
                if (tile.Type == TileType.blocked)
                {
                    if (!blockedList.Contains(tile))
                    {
                        blockedList.Add(tile);
                    }
                }
                if (tile.Type == TileType.open)
                {

                    if (!openList.Contains(tile))
                    {
                        openList.Add(tile);
                    }
                    var adjacentTiles = grid.AdjacentTiles(tile, true);
                    for (int m = 0; m < adjacentTiles.Count; m++)
                    {
                        if (adjacentTiles[m].tileObject == null)
                        {
                            outOfBounds = true;
                            continue;
                        }
                        if (!searchHash.Contains(adjacentTiles[m]) &&
                            !blockedList.Contains(adjacentTiles[m]) &&
                            !openList.Contains(adjacentTiles[m]))
                        {
                            searchHash.Add(adjacentTiles[m]);
                        }
                    }
                }

                searchHash.Remove(tile);
            }
            else
            {
                //done
                //Debug.Break();
                FinishedAlgorithm();
                return;
            }

            //if (!outOfBounds)
            //{
            SearchForMore();
            //}
        }

        public void FinishedAlgorithm()
        {
            if (finishedCallback != null)
            {
                finishedCallback(this);
            }
        }
    }
}
