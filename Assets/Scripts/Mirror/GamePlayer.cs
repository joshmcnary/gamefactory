﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

/*
 * MIRROR NOTES
 * 
 * classes need default constructors to be used as parameters in commands
 * 
 */

public class GamePlayer : NetworkBehaviour {

    [SyncVar]
    public int Index;

    GameObject Cursor;

    int count = 0;

    // this has server authority
    public void Init() {
        //UnityEngine.Cursor.visible = false;
        /*
        Cursor = Instantiate(NetworkManager.singleton.spawnPrefabs[0]);
        NetworkServer.Spawn(Cursor);
        */
        if (Index == 1) {
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            Material blue = Resources.Load("Materials/PlayerBlue") as Material;
            renderer.materials = new Material[1] { blue };
        }

        /*
        WorldStateManager.access.UpdateHighlightedMapNode += delegate (Tile tile) {
            //CmdMoveCursor(tile);
        };
        */

        //CmdCreateEntity(EntityType.Cannon);
        //EntityManager.access.CreateEntityTemp(EntityType.Cannon);

    }

    private void Update() {
        //string currentScene = SceneManager.GetActiveScene().name;
        CmdTileUpdateTest();
    }

    //
    [Command]
    void CmdTileUpdateTest() {

        if (count != WorldStateManager.access.MessagesRate) {
            count++;
            return;
        }

        int messageCount = WorldStateManager.access.MessagesCount;
        for (int i = 0; i < messageCount; i++) {
            int randX = Random.Range(0, GridData.access.Width);
            int randZ = Random.Range(0, GridData.access.Height);
            BlockingObject blocker = Random.Range(0, 2) == 0 ? BlockingObject.Tree : BlockingObject.Stone;
            GridData.access.GetTileNew(randX, randZ).SetBlockingObject(blocker);
        }

        Debug.Log(messageCount + " messages sent");

        count = 0;
        WorldStateManager.access.MessagesRate--;
        WorldStateManager.access.MessagesCount++;


    }

    //
    /*
    public void Init() {

        /*
        if (player.Index == 0) {
            MeshRenderer renderer = player.GetComponent<MeshRenderer>();
            Material blue = Resources.Load("Materials/PlayerBlue") as Material;
            renderer.materials = new Material[1] { blue };
        }
        */

    //}

    [Command]
    public void CmdCreateEntity(EntityType type, Tile tile) {
        bool tileHasBlockable = tile.BlockableEntityId != 0;
        if (!tileHasBlockable) {
            EntityManager.access.AddNewEntityToGame(type, tile.coords);
        } else {
            Entity entityToRemove = EntityManager.access.GetEntityById(tile.BlockableEntityId);
            entityToRemove.RemoveFromMap();
        }
    }

    [Command]
    public void CmdSetTile(int x, int z) {
        Tile tile = GridData.access.GetTileNew(new Point(x, z));
        int randomTileType = UnityEngine.Random.Range(0, 2);
        if (randomTileType == 0) {
            tile.type = TileType.Grass;
        } else {
            tile.type = TileType.Water;
        }
        GridData.access.SetTile(x, z, tile);
    }
}

