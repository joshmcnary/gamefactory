﻿/* 
    This class should contain anything relating to the camera. Currently, this is
    - Camea Movement

    Setting up a camera requires a source Camera and a target Gameobject. A Physics Raycaster should be attached to the source Camera,
    which will invoke mouse input events, and listened to in specific Monobehaviours such as Entity->OnMouseDown.

*/

using UnityEngine;
using System.Collections;

using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour {

    [Header("Initial Target")]
    public float initialX;
    public float initialZ;

    [Header("Movement Controls")]
    public float moveUnits = 20f;
    
    [Header("Zoom Controls")]
    public float minFov = 1f;
    public float maxFov = 80f;
    public float sensitivity = 2f;
    
    public static float diagonalSpeedMultiplier = 0.7071f;

    public Vector3 LastValidPosition;       // used to keep camera in bounds of map

    void Update () {

        LastValidPosition = transform.position;

        Vector2 dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        //Camera Movement
        float speed = moveUnits * Time.fixedDeltaTime;
        if (dir.x != 0 && dir.y !=0) { speed *= diagonalSpeedMultiplier; }

        float xDistance = speed * dir.x * moveUnits;
        float zDistance = speed * dir.y * moveUnits;

        transform.Translate(
            xDistance,    
            zDistance,
            0
        );

        //Zooming
        /*
        if (WorldStateManager.access.GameState == GameState.Build) {
            float fov = Camera.main.orthographicSize;
            fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
            fov = Mathf.Clamp(fov, minFov, maxFov);
            Camera.main.orthographicSize = fov;
        }
        */
    }

    // sets the point which will be at the center of the display (roughly)
    public void SetTargetPoint(Vector3 targetPoint) {
        transform.position = targetPoint + new Vector3(-8.5f, 0, -8.5f);
    }
}
