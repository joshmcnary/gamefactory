﻿using UnityEngine;
using UnityEditor;

public class WorldSettingsMenuItem {

    // this seems to be the simplest method to replicate since scriptableobject.createinstance cannot use generic types
    // this means the settings asset needs to be instantiated first, then passed as a parameter in this method
    private static void CreateScriptableObject(ScriptableObject asset) {

        WorldSettings settings = (WorldSettings)asset;

        AssetDatabase.CreateAsset(asset, "Assets/Settings/World/WorldSettings.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;

        //Debug.Log(settings.type);

    }

    [MenuItem("World Settings/Standard")]
    public static void StandardSettings() {
        WorldSettings asset = ScriptableObject.CreateInstance<WorldSettings>();
        CreateScriptableObject(asset);
    }
}
