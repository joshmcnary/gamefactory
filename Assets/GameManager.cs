﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public Transform leftRacketSpawn;
    public Transform rightRacketSpawn;
    public GameObject ballPrefab;
    GameObject ball;

    // Start is called before the first frame update
    /*
    void Start()
    {
        Transform start = leftRacketSpawn;
        //Transform start = numPlayers == 0 ? leftRacketSpawn : rightRacketSpawn;
        //GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        //NetworkServer.AddPlayerForConnection(conn, player);

        GameObject player1 = Instantiate(playerPrefab, leftRacketSpawn.position, leftRacketSpawn.rotation);
        NetworkServer.AddPlayerForConnection(NetworkServer.connections[0], player1);

        GameObject player2 = Instantiate(playerPrefab, rightRacketSpawn.position, rightRacketSpawn.rotation);
        //NetworkServer.AddPlayerForConnection(NetworkServer.connections[1], player2);


        // spawn ball if two players
        if (true) {
            ball = Instantiate(ballPrefab);
            NetworkServer.Spawn(ball);
        }
    }
    */
}
